//
//  ProfileCollectionViewCell.swift
//  Recruitment
//
//  Created by Apple on 10/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var user_image: UIImageView!
    
    @IBOutlet weak var user_name: UILabel!
    
    @IBOutlet weak var user_status: UILabel!
    
    @IBOutlet weak var user_type: UILabel!
    
    @IBOutlet weak var user_email: UILabel!
    
    @IBOutlet weak var delete_button: UIButton!
    
    @IBOutlet weak var edit_button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
