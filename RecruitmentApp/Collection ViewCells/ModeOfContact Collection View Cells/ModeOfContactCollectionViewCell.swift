//
//  ModeOfContactCollectionViewCell.swift
//  Recruitment
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ModeOfContactCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var delete_button: UIButton!
    @IBOutlet weak var edit_button: UIButton!
    @IBOutlet weak var main_view: UIView!
    
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
