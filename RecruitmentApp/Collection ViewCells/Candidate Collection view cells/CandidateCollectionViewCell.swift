//
//  CandidateCollectionViewCell.swift
//  Recruitment
//
//  Created by Apple on 12/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CandidateCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var twitter_button: UIButton!
    @IBOutlet weak var linkedin_button: UIButton!
    @IBOutlet weak var whatsapp_button: UIButton!
    @IBOutlet weak var facebook_button: UIButton!
    @IBOutlet weak var user_whatsapp: UIButton!
    @IBOutlet weak var user_phone: UIButton!
    @IBOutlet weak var user_gender: UILabel!
    @IBOutlet weak var user_name: UILabel!
    
    @IBOutlet weak var user_whatsapp_label: UILabel!
    @IBOutlet weak var user_phone_label: UILabel!
    @IBOutlet weak var user_email: UILabel!
    @IBOutlet weak var user_image: UIImageView!
    @IBOutlet weak var view_more_button: UIButton!
    
    @IBOutlet weak var user_current_location: UILabel!
    @IBOutlet weak var main_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
