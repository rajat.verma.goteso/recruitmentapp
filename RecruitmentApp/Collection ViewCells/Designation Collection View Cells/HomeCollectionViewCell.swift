//
//  HomeCollectionViewCell.swift
//  Recruitment
//
//  Created by Apple on 09/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var main_view: UIView!
    
    @IBOutlet weak var user_image: UIImageView!
    
    @IBOutlet weak var user_title_label: UILabel!
    
    @IBOutlet weak var delete_button: UIButton!
    
    @IBOutlet weak var edit_button: UIButton!
    
    @IBOutlet weak var buttons_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      
       
    }

}
