//
//  HomeCell.swift
//  Recruitment
//
//  Created by Apple on 12/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {

    @IBOutlet weak var designation_title: UILabel!
    @IBOutlet weak var main_view: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        main_view.rightBottomCorner()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        main_view.rightBottomCorner()
    }
    
}
