//
//  AppDelegate.swift
//  RecruitmentApp
//
//  Created by Apple on 24/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
let REGULAR_FONT = "Open Sans"
let REGULAR_COLOR = "#3886C3"
var api_prefix = "c/"
var refresh_token = ""
var appType =  ""
var access_token = ""
var token_type = ""
var localTimeZoneName = ""
var url_type = ""
var BASE_URL = "http://192.168.1.30/laravel/laravel/Recruitment_2/public/"
var store_id : Int!
var BASE_IMAGE_URL = "http://192.168.1.30/laravel/laravel/Recruitment_2/public/"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
let userDefaults = UserDefaults.standard

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        print("")
        if userDefaults.object(forKey: "user_data") == nil {
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
        }
        else {
            
            let userEmail = userDefaults.value(forKey: "User_Email_Id")
            access_token = userDefaults.value(forKey: "access_token") as! String
            refresh_token = userDefaults.value(forKey: "refresh_token") as! String
            token_type = userDefaults.value(forKey: "token_type") as! String
            
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let nav = UINavigationController(rootViewController: homeVC)
            
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
            
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

