//
//  AddRecordTableViewCell.swift
//  Recruitment
//
//  Created by Apple on 04/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AddRecordTableViewCell: UITableViewCell {

    
    @IBOutlet weak var img_icon: UIImageView!
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var record_text_field: UITextField!
    
    @IBOutlet weak var experience_view: UIView!
    @IBOutlet weak var months_text_field: UITextField!
    @IBOutlet weak var years_text_field: UITextField!
    let view = UIView()

    @IBOutlet weak var years_button: UIButton!
    
    @IBOutlet weak var months_button: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
   
//        let textField = SkyFloatingLabelTextField(frame: CGRect(x: 10, y: 10, width: 200, height: 45))
//        textField.placeholder = "Name"
//        textField.title = "Your full name"
//        self.view.addSubview(textField)
     
    
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        record_text_field.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
       
    }
    
    
}
