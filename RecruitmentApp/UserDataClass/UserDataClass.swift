//
//  UserDataClass.swift
//  ApiTaskTwo
//
//  Created by Apple on 19/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

class UserdataClass : NSObject, NSCoding {
    
   
    var User_Email_Id : String!
    var User_ID : Int!
    var User_Type : Int!
    var User_Name : String!
     var User_Pic : String!
   
  //  var user_referral_code : String!
    
    
    
    init(User_Email_id: String, User_id : Int , User_type : Int , User_name: String , User_pic: String) {
        
        self.User_Email_Id = User_Email_id
        self.User_ID = User_id
        self.User_Type = User_type
        self.User_Name = User_name
        self.User_Pic = User_pic
    //    self.user_referral_code = user_referral_code
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        
        let user_email_id = aDecoder.decodeObject(forKey: "User_Email_Id") as! String
        let user_id = aDecoder.decodeObject(forKey: "User_ID")as! Int
        let user_type = aDecoder.decodeObject(forKey: "User_Type")as! Int
         let user_name = aDecoder.decodeObject(forKey: "User_Name") as! String
         let user_pic = aDecoder.decodeObject(forKey: "User_Pic") as! String
    
     //   let user_refferal_code = aDecoder.decodeObject(forKey: "User_refferal_code") as! String
        
        
        self.init(User_Email_id: user_email_id, User_id: user_id , User_type : user_type , User_name : user_name , User_pic : user_pic)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(User_Email_Id, forKey: "User_Email_Id")
        aCoder.encode(User_ID, forKey: "User_ID")
        aCoder.encode(User_Type, forKey: "User_Type")
         aCoder.encode(User_Name, forKey: "User_Name")
         aCoder.encode(User_Pic, forKey: "User_Pic")
        
    //    aCoder.encode(user_referral_code, forKey: "User_referral_code")
      
    }
    
   
    
    
    
}
