//
//  WebService.swift
//  Dry Clean City
//
//  Created by Apple on 06/06/18.
//  Copyright © 2018 Kishore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


class WebService: NSObject {

    
    class func showAlert ()
    {
        let alert = UIAlertController(title: nil, message: " \n\n\n\n\n\n\n", preferredStyle: .alert)
        let uiView = UIView(frame: CGRect(x: alert.view.frame.origin.x, y: 15, width: 250, height: 150))
      
        let imageV = UIImageView(frame: CGRect(x: uiView.center.x - 40, y: 10, width: 100, height: 90))
        
        imageV.image = #imageLiteral(resourceName: "internet-placeholder")
        imageV.contentMode = .scaleAspectFill
        uiView.addSubview(imageV)
        let msgLbl = UILabel(frame: CGRect(x: uiView.frame.origin.x + 20, y: imageV.frame.size.height + 30, width: 230, height: 50))
        msgLbl.font = UIFont(name: "Open Sans", size: 17)
        msgLbl.textAlignment = .center
        msgLbl.numberOfLines = 2
        msgLbl.textColor = UIColor(red: 108/255.0, green: 108/255.0, blue: 108/255.0, alpha: 1)
        msgLbl.text = "Check the Internet connection"
        let fonD:UIFontDescriptor = msgLbl.font.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits.traitBold)!
        
        msgLbl.font = UIFont(descriptor: fonD, size: 17)
        uiView.addSubview(msgLbl)
        alert.view.addSubview(uiView)
        
        alert.addAction(UIAlertAction(title:  "", style: .default, handler: { (action) in
           return
        }))
        let viewController = UIApplication.shared.keyWindow?.rootViewController
        
        let popPresenter = alert.popoverPresentationController
        popPresenter?.sourceView = viewController?.view
        popPresenter?.sourceRect = (viewController?.view.bounds)!
        viewController?.present(alert, animated: true, completion: nil)
    }
    
    class func authenticationFunction(isForLogin:Bool)
    {
        
        
        if isForLogin {
            return
        }
        
        if  UserDefaults.standard.value(forKey: "expireDate") != nil
        {
            let currentDate = Date()
            let expireDate = (UserDefaults.standard.value(forKey: "expireDate") as! Date)
            
            if UserDefaults.standard.value(forKey: "refresh_token") != nil
            {
                refresh_token = (UserDefaults.standard.value(forKey: "refresh_token") as! String)
            }
            if currentDate > expireDate
            {
                
                let api_name = APINAME().ACCESS_TOKEN_API
                let param = ["grant_type":"refresh_token","client_secret":"f36F4ZZN84kWE9cwYbFj2Y6er5geY9OBXF3hEQO4","client_id":"2","refresh_token":refresh_token]
                
                var request = URLRequest(url: NSURL(string: BASE_URL.appending(api_name))! as URL)
                print(BASE_URL.appending(api_name))
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                
                do
                {
                    // json format
                    let body = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
                    
                    let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                    
                    print("Post Data -> \(String(describing: postString))")
                    
                    request.httpBody = body
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
                
                
                var response: URLResponse?
                var resultDictionary: NSDictionary!
                do
                {
                    let urlData = try NSURLConnection.sendSynchronousRequest(request, returning: &response)
                    resultDictionary = try (JSONSerialization.jsonObject(with: urlData, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary)
                    print(resultDictionary)
                    
                    if resultDictionary["error"] != nil
                    {
                        //Check for "Unauthenticated access"
                      
                            
                            NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                            return
                       
                    }
                    else
                    {
                        access_token = (resultDictionary["access_token"] as! String)
                        refresh_token = (resultDictionary["refresh_token"] as! String)
                        token_type  = (resultDictionary["token_type"] as! String)
                        let expireTime = (resultDictionary["expires_in"] as! NSNumber)
                        let expireDate = Date().addingTimeInterval(TimeInterval(exactly: expireTime)!)
                        UserDefaults.standard.setValue(access_token, forKey: "access_token")
                        UserDefaults.standard.setValue(expireDate, forKey: "expireDate")
                        UserDefaults.standard.setValue(refresh_token, forKey: "refresh_token")
                        UserDefaults.standard.setValue(token_type, forKey: "token_type")
                        return
                    }
                    
                    
                }
                catch
                {
                    
                }
                
                
            }
        }
        return
    }
    
    
    class func requestGetUrlWithoutParameters(strURL:String,is_loader_required:Bool, success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        print(appType)
        
        
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
          if is_loader_required
          {
        SVProgressHUD .show()
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setRingNoTextRadius(14.0)
            }
            
            
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
            
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
            }
            catch let error as NSError
            {
                print(error)
            }
            
            authenticationFunction(isForLogin: false)
            
            
            let headers = [
                "class_identifier": "",
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            
            
            var request = URLRequest(url: NSURL(string: BASE_URL.appending(strURL))! as URL)
            print(BASE_URL.appending(strURL))
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
        print(BASE_URL.appending(strURL))
        
            Alamofire.request(request)
                .responseString { response in
                    // do whatever you want here
                    switch response.result {
                    case .success(_):
                        if let data = response.data
                        {
                             SVProgressHUD.dismiss()
                            print(JSON(data))
                        
                     //Check for "Unauthenticated access"
                            if let dataDictionary = JSON(data).dictionaryObject
                            {
                                if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                                {
                                    if status == 1001{
                                        NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                        return
                                    }
                                    
                                    
                                }
                            }
                            
                            
                           
                            if let dataDictionary = JSON(data).dictionaryObject
                            {
                                if dataDictionary["status_code"] == nil
                                {
                                    success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                                }
                                else
                                {
                                     success(dataDictionary as NSDictionary)
                                }
                              
                                }
                            if let dataArray = JSON(data).arrayObject
                            {
                                success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                            }
                        }
                        break
                        
                    case .failure(_):
                        SVProgressHUD.dismiss()
                        failure(response.error.debugDescription )
                        print(response.error.debugDescription)
                        break
                    }
            }
           }
            
        else
        {
            showAlert()
            return
        }
        
    }
    
    
    class func requestGetUrl(strURL:String, params:NSDictionary,is_loader_required:Bool, success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            print(appType)
            let header = ["class_identifier": appType]
            
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
            }
            catch let error as NSError
            {
                print(error)
            }
            
            
        print(BASE_URL.appending(strURL))
        Alamofire.request(BASE_URL.appending(strURL), method: HTTPMethod.get, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: header).responseData { (response:DataResponse<Data>) in
            
            switch(response.result) {
                
            case .success(_):
                if let data = response.data
                {
                    //Check for "Unauthenticated access"
                     SVProgressHUD.dismiss()
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                        {
                            if status == 1001{
                                NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                return
                            }
                            
                            
                        }
                    }
                    
                    
                   
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if dataDictionary["status_code"] == nil
                        {
                            success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                        }
                        else
                        {
                            success(dataDictionary as NSDictionary)
                        }
                    }
                    if let dataArray = JSON(data).arrayObject
                    {
                        success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                    }
                }
                break
                
            case .failure(_):
                SVProgressHUD.dismiss()
                failure(response.error.debugDescription )
                break
            }
            
            
        }
        }
            
        else
        {
            showAlert()
            return
        }
    }
    
    
    
    
    class func requestGetUrlForCheckPort(strURL:String,is_loader_required:Bool, success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        print(appType)
        
        
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
            
            
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
            
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
            }
            catch let error as NSError
            {
                print(error)
            }
            
            authenticationFunction(isForLogin: false)
            
            
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            var  BaseUrl = ""
            if url_type == "port"
            {
                BaseUrl = BASE_URL
                // BaseUrl = "http://139.59.86.194:4105/v2/"
            }
            else
            {
                BaseUrl = "https://www.ordefy.com/api/"
            }
            
            
            var request = URLRequest(url: NSURL(string: BaseUrl.appending(strURL))! as URL)
            print(BaseUrl.appending(strURL))
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers
            
            
            Alamofire.request(request)
                .responseString { response in
                    // do whatever you want here
                    switch response.result {
                    case .success(_):
                        if let data = response.data
                        {
                            //Check for "Unauthenticated access"
                            SVProgressHUD.dismiss()
                            if let dataDictionary = JSON(data).dictionaryObject
                            {
                                if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                                {
                                    if status == 1001{
                                        NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                        return
                                    }
                                    
                                    
                                }
                            }
                            
                            
                            if let dataDictionary = JSON(data).dictionaryObject
                            {
                                
                                if dataDictionary["status_code"] == nil
                                {
                                    success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                                }
                                else
                                {
                                    success(dataDictionary as NSDictionary)
                                }
                            }
                            if let dataArray = JSON(data).arrayObject
                            {
                                success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                            }
                            else
                            {
                                
                            }
                        }
                        break
                        
                    case .failure(_):
                        SVProgressHUD.dismiss()
                        print(response.error as Any)
                        let viewController = UIApplication.shared.keyWindow?.rootViewController
                        //viewController?.view.makeToast(response.error?.localizedDescription, duration: 1.0, position: .bottom)
                        failure(response.error.debugDescription )
                        break
                    }
            }
        }
            
        else
        {
            showAlert()
            return
        }
        
    }
    
    
    
    class func requestPostUrl(strURL:String, params:NSDictionary,is_loader_required:Bool, success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        print(appType)
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
        
        
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
            print(BASE_URL.appending(strURL))
            print(params )
            
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
            }
            catch let error as NSError
            {
                print(error)
            }
            if !strURL.contains("oauth/token")
            {
                authenticationFunction(isForLogin: false)
                
            }
          
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
        
        Alamofire.request(BASE_URL.appending(strURL), method: HTTPMethod.post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: headers).responseData { (response:DataResponse<Data>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.data
                {
                    //Check for "Unauthenticated access"
                    SVProgressHUD.dismiss()
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                        {
                            if status == 1001{
                                NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                return
                            }
                            
                        }
                    }
                    if strURL.contains("oauth/token") {
                        
                        let dataDictionary = JSON(data).dictionaryObject
                        
                        success(dataDictionary! as NSDictionary)
                    }
                    
                    else {
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if dataDictionary["status_code"] == nil && dataDictionary["token_type"] == nil
                        {
                            success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                        }
                        else
                        {
                            success(dataDictionary as NSDictionary)
                        }
                    }
                        
                    
                    if let dataArray = JSON(data).arrayObject
                    {
                        success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                    }
                        
                    }
                }
                break
                
            case .failure(_):
                 SVProgressHUD.dismiss()
                failure(response.error.debugDescription )
                break
            }
           
        }
        }
            
        else
        {
            showAlert()
            return
        }
    }
    
    
    
    class func requestDelUrl(strURL:String,is_loader_required:Bool, params:[String:Any], success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        print(appType)
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            
            
        
            if is_loader_required
            {
                SVProgressHUD.show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
       // let url = "http://demo.goteso.com/food/public/" + strURL
        
        print(BASE_URL.appending(strURL))
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
            }
            catch let error as NSError
            {
                print(error)
            }
            
            
            authenticationFunction(isForLogin: false)
            
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            
        Alamofire.request(BASE_URL.appending(strURL), method: HTTPMethod.delete, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response:DataResponse<Data>) in
            switch(response.result) {
            case .success(_):
                if let data = response.data
                {
                    print(data)
                    //Check for "Unauthenticated access"
                      SVProgressHUD.dismiss()
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                        {
                            if status == 1001{
                                NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                return
                            }
                            
                            
                        }
                    }
                    
                  
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if dataDictionary["status_code"] == nil
                        {
                            success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                        }
                        else
                        {
                            success(dataDictionary as NSDictionary)
                        }
                    }
                    if let dataArray = JSON(data).arrayObject
                    {
                        success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                    }
                    
                }
                break
                
            case .failure(_):
                SVProgressHUD.dismiss()
                failure(response.error.debugDescription )
                break
            }
            
        }
        }
            
        else
        {
            showAlert()
            return
        }
        
    }
    
    
    class func requestPostUrlWithJSONDictionaryParameters(strURL:String,is_loader_required:Bool, params:[String:Any], success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        
        print(appType)
       
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            
        
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
        print(BASE_URL.appending(strURL))
        print(params )
       
        do
        {
            // json format
            let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
            
            print("Post Data -> \(String(describing: postString))")
            
        }
        catch let error as NSError
        {
            print(error)
        }
        
            var isForLogin = false
            
            if strURL.contains("user-login")
            {
                isForLogin = true
            }
            else
            {
                isForLogin = false
            }
            
            if !strURL.contains("user-login")
            {
                authenticationFunction(isForLogin: isForLogin)
                
            }
            
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            
        Alamofire.request(BASE_URL.appending(strURL), method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response:DataResponse<Data>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.data
                {
                    print(JSON(data))
                    //Check for "Unauthenticated access"
                       SVProgressHUD.dismiss()
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                        {
                            if status == 1001{
                                NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                return
                            }
                            
                            
                        }
                    }
                    
                  
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if dataDictionary["status_code"] == nil
                        {
                            success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                        }
                        else
                        {
                            success(dataDictionary as NSDictionary)
                        }
                    }
                    if let dataArray = JSON(data).arrayObject
                    {
                        success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                    }
                }
                break
                
            case .failure(_):
                 SVProgressHUD.dismiss()
                failure(response.error.debugDescription )
                break
            }
            
            
        }
        }
            
        else
        {
            showAlert()
            return
        }
    }
    
    
    
    
    class func requestPUTUrlWithJSONArrayParameters(strURL:String,is_loader_required:Bool, params:NSArray, success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        
        print(appType)
       
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            
        
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
            authenticationFunction(isForLogin: false)
            
            
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            
         print(BASE_URL.appending(strURL))
        var request = URLRequest(url: NSURL(string: BASE_URL.appending(strURL))! as URL)
        print(BASE_URL.appending(strURL))
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = headers
        do
        {
            // json format
                let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
                request.httpBody = body
           
        }
        catch let error as NSError
        {
            print(error)
        }
        
        Alamofire.request(request)
            .responseString { response in
                // do whatever you want here
                switch response.result {
                case .success(_):
                    if let data = response.data
                    {
                        print(JSON(data))
                        //Check for "Unauthenticated access"
                         SVProgressHUD.dismiss()
                        if let dataDictionary = JSON(data).dictionaryObject
                        {
                            if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                            {
                                if status == 1001{
                                    NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                    return
                                }
                                
                                
                            }
                        }
                        
                       
                        if let dataDictionary = JSON(data).dictionaryObject
                        {
                            if dataDictionary["status_code"] == nil
                            {
                                success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                            }
                            else
                            {
                                success(dataDictionary as NSDictionary)
                            }
                        }
                        if let dataArray = JSON(data).arrayObject
                        {
                            success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                        }
                    }
                    break
                    
                case .failure(_):
                     SVProgressHUD.dismiss()
                    failure(response.error.debugDescription )
                     print(response.error.debugDescription)
                    break
                }
        }
        }
            
        else
        {
            showAlert()
            return
        }
    }
    
    
    
    class func requestPUTUrlWithJSONDictionaryParameters(strURL:String,is_loader_required:Bool, params:[String:Any], success:@escaping (_ response:NSDictionary) -> (), failure:@escaping (String) -> ()) {
        
        print(appType)
        
        if Connectivity.isConnectedToInternet {
            print("Yes! internet is available.")
            
            
            if is_loader_required
            {
                SVProgressHUD .show()
                SVProgressHUD.setDefaultStyle(.custom)
                SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
                SVProgressHUD.setRingNoTextRadius(14.0)
            }
        
        print(BASE_URL.appending(strURL))
            var request = URLRequest(url: NSURL(string: BASE_URL.appending(strURL))! as URL)
            print(BASE_URL.appending(strURL))
            
            do
            {
                // json format
                let body = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                
                let postString = NSString(data: body, encoding: String.Encoding.utf8.rawValue)
                
                print("Post Data -> \(String(describing: postString))")
                
                request.httpBody = body
                
            }
            catch let error as NSError
            {
                print(error)
            }
        print(params )
        
            authenticationFunction(isForLogin: false)
            
            
            let headers = [
                "class_identifier": appType,
                "timezone":localTimeZoneName,
                //"Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "\(token_type) \(access_token)"
                
            ]
            print(headers)
            
        Alamofire.request(BASE_URL.appending(strURL), method: HTTPMethod.put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response:DataResponse<Data>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.data
                {
                    print(JSON(data))
                    //Check for "Unauthenticated access"
                     SVProgressHUD.dismiss()
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if let status = ((dataDictionary as NSDictionary)["status_code"] as? NSNumber)
                        {
                            if status == 1001{
                                NotificationCenter.default.post(name: NSNotification.Name.init("LogoutNotification"), object: nil)
                                return
                            }
                            
                            
                        }
                    }
                    
                   
                    if let dataDictionary = JSON(data).dictionaryObject
                    {
                        if dataDictionary["status_code"] == nil
                        {
                            success(NSDictionary(dictionaryLiteral: ("status_code",1008),("message","")))
                        }
                        else
                        {
                            success(dataDictionary as NSDictionary)
                        }
                    }
                    if let dataArray = JSON(data).arrayObject
                    {
                        success(NSDictionary(dictionaryLiteral:  ("data",dataArray)))
                    }
                }
                break
                
            case .failure(_):
                SVProgressHUD.dismiss()
                failure(response.error.debugDescription )
                break
            }
            
            
        }
        }
            
        else
        {
            showAlert()
            return
        }
    }

}

class APINAME {
   
    //SignUp Api
    let SIGN_UP_OTP = api_prefix + "signup-otp"
    let SIGN_UP_API = api_prefix + "signup"
    //Store Api
    let STORES_API = api_prefix + "stores" //Done
    let GET_REVIEWS_LIST = api_prefix + "reviews"
   
    //Category API
    let CATEGORY_API = api_prefix + "product/categories" //Done
    
    
    let LOGOUT = api_prefix + "logout" //Done
    
    let USER_FORM_API = api_prefix + "profile/form" //Done
    
    //Change Password
    let UPDATE_PASSWORD = api_prefix + "update-password" //Done
    
    //Reset Password
    let FORGOT_PASSWORD = api_prefix + "forgot-password" //Done
    let VERIFY_OTP = api_prefix + "forgot-password/verify" //Done
    let RESET_PASSWORD = api_prefix + "reset-password" //Done
    
    //Item Related API
    let ITEM_API = api_prefix + "products" //Done
    let ITEM_IMAGE_API = api_prefix + "product-image" //Done
    let ITEM_EDIT_FORM = api_prefix + "product/form" //Done
    let ITEM_VARIANT = api_prefix + "product-variant" //Done
    let GET_TAGS = api_prefix + "product/tags" //Done
    
    //ORDER API
    let ORDERS_API = api_prefix + "orders" //Done
    let ORDER_CANCEL = api_prefix + "order/cancel" //Done
    let CREATE_ORDER_NOTE = api_prefix + "order/notes" //Done
    let CHATTING_API = api_prefix + "order/messages" //Done
    
    let FAQ_API = api_prefix + "faq" //Done
    let SETTINGS_API = api_prefix + "settings" //Done
    let LOGIN_API = "api/login" //Done
 
    //Access Token API
    let ACCESS_TOKEN_API = "oauth/token" //Done
    //Refresh Token API
    let REFRESH_TOKEN_API = "oauth/token"
    
    let CHECK_TEAM = "check_team" //Done
    
    
    
  //  let EARNINGS_LIST = api_prefix + "earning-list" //Delete//
    
    
    // Products Api
    
    let PRODUCTS_API = api_prefix + "products"
    
   //  Order Payment Summary
    
    let ORDER_PAYMENT_API = api_prefix + "order/payment-summary"
    
    // Order Form Api
    
    let ORDER_FORM_API = api_prefix + "order/form"
    
    let PAYMENT_GATEWAY_API = api_prefix + "payment-gateway"
    
    let LIST_ALL_COUPONS_API = api_prefix + "coupons"
    
    let APPLY_COUPON_API = api_prefix + "order/apply-coupon"
    
    let POINTS_API = api_prefix + "order/points"
    
    let DELIVERY_ADDRESS_API = api_prefix + "address"
    
    let TIME_API = api_prefix + "order/timeslots"
    
    
    
    let DESIGNATION_LIST_API = "api/d/designations"
    let ADD_NEW_DESIGNATION_API = "api/d/create"
    let UPDATE_DESIGNATION_API = "api/d/update"
    let DELETE_DESIGNATION_API = "api/d/delete"
    let UPLOAD_IMAGE_API = "api/upload"
    let PROFILE_API = "api/users"
    let UPDATE_PROFILE_API = "api/u/update"
    let DELETE_PROFILE_API = "api/u/delete"
    let ADD_NEW_PROFILE_API = "api/u/create"
    let MODE_OF_CONTACT_API = "api/m/modes"
    let ADD_NEW_MODE_OF_CONTACT_API = "api/m/create"
    let UPDATE_MODE_OF_CONTACT_API = "api/m/update"
    let DELETE_MODE_OF_CONTACT_API = "api/m/delete"
    let FORGOT_PASSWORD_API = "api/forgot"
    let VERIFY_OTP_API = "api/verify"
    let CHANGE_PASSWORD_API = "api/reset"
    let CANDIDATE_API = "api/d/candidate"
    let GET_CANDIDATE_API = "api/d/candidate/data"
    let CREATE_CANDIDATE_API = "api/create"
    let UPDATE_CANDIDATE_API = "api/update"
    let Delete_CANDIDATE_API = "api/delete"
    let CHANGE_PASSWORD_API_ = "api/change_password"
    
    
}


class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
