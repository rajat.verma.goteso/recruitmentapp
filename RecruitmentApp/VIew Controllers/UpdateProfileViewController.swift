//
//  SignUpViewController.swift
//  Recruitment
//
//  Created by Apple on 09/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class UpdateProfileViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    
    @IBOutlet weak var user_image: UIImageView!
    
    @IBOutlet weak var edit_user_image: UIButton!
    
    
    @IBOutlet weak var switch_button: UISwitch!
    @IBOutlet weak var status_Label: UILabel!
    @IBOutlet weak var user_mobile_text_field: UITextField!
    @IBOutlet weak var username_text_field: UITextField!
    
    @IBOutlet weak var useremail_text_field: UITextField!
    
    @IBOutlet weak var usertype_text_field: UITextField!
    
    @IBOutlet weak var userstatus_text_field: UITextField!
    
    
    @IBOutlet weak var user_type_button: UIButton!
    @IBOutlet weak var update_button: UIButton!
    
     let button = UIButton(type: .custom)
    var picker  = UIPickerView()
     var toolBar = UIToolbar()
    var components = ["HR Manager","Recruiter"]
    
    let show_password_button = UIButton(type: .custom)
    let hide_password_button = UIButton(type: .custom)
    
    
    var profile_ID : NSNumber!
    var profile_Array = [NSDictionary] ()
    var profile_dict = NSDictionary()
    var profile_title = ""
    var user_type = NSNumber ()
    var user_status = NSNumber ()
    
    var name = ""
    var image_url = ""
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
    
    
    @objc func switch_button_action(_ sender: UISwitch) {
        
        if switch_button.isOn {

            status_Label.text = "Enabled"
        displayAlert(msg: "Enabled")
        }
        else{

             status_Label.text = "Disabled"
            displayAlert(msg: "Disabled")
        }
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(profile_dict)
        
        status_Label.underline()
        switch_button.addTarget(self, action: #selector(switch_button_action(_:)), for: .touchUpInside)
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Update Profile"
        
        username_text_field.text = (profile_dict.object(forKey: "name") as! String)
      //  username_text_field.delegate = self
        useremail_text_field.text = (profile_dict.object(forKey: "email") as! String)
      //  useremail_text_field.delegate = self
        user_mobile_text_field.text = ("\((profile_dict.object(forKey: "mobile") as! String))")
        //usertype_text_field.text = (profile_dict.object(forKey: "user_type") as! String)
       // usertype_text_field.delegate = self
//        userstatus_text_field.text = (profile_dict.object(forKey: "status") as! String)
//        userstatus_text_field.delegate = self
        
        
        if profile_dict.object(forKey: "user_type") as! NSNumber == 1 {
            
            usertype_text_field.text = "Admin"
         //   usertype_text_field.delegate = self
        }
        if profile_dict.object(forKey: "user_type") as! NSNumber == 2 {
            
            usertype_text_field.text = "HR Manager"
         //   usertype_text_field.delegate = self
        }
        if profile_dict.object(forKey: "user_type") as! NSNumber == 3 {
            usertype_text_field.text = "HR Recruiter"
         //   usertype_text_field.delegate = self
        }
        
        if profile_dict.object(forKey: "status") as! NSNumber == 1 {
            
            switch_button.isOn = true
            status_Label.text = "Enabled"
        }
        
        if profile_dict.object(forKey: "status") as! NSNumber == 0 {
            
            switch_button.isOn = false
            status_Label.text = "Disabled"
        }
        
        
        
        if (profile_dict.object(forKey: "profile_pic") as! String).isEmpty == true {
            
            //   cell.userImageView.setImage(string: user_data.user_first_name! + " " + user_data.user_last_name!)
            print("Image hai ni")
            
        }
        else
        {
            if imagePath.isEmpty
            {
                imagePath = (profile_dict.object(forKey: "profile_pic") as! String)
            }
            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
            print(image_url as Any)
            
            user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
        }
        
        if userImage != nil
        {
            user_image.image = userImage
            user_image.contentMode = .scaleAspectFit
        }
        
        
        
     update_button.addTarget(self, action: #selector(updateProfile_API(_:)), for: .touchUpInside)
        
//        user_image.layer.cornerRadius = user_image.frame(CGRect(x: 0, y: 0, width: self.user_image.frame.size.width / 2, height: user_image.frame.size.height/2))
//        edit_user_image.layer.cornerRadius = edit_user_image.frame(CGRect(x: 0, y: 0, width: view.frame.width/2, height: user_image.frame.height/2))
    }
    
    override func viewDidLayoutSubviews() {
        

    
        update_button.layer.masksToBounds = true
        update_button.layer.cornerRadius = 3
       
        user_image.layer.masksToBounds = true
        edit_user_image.layer.masksToBounds = true
        user_image.layer.cornerRadius = 65
        edit_user_image.layer.cornerRadius = 24
        
        edit_user_image.addTarget(self, action: #selector(edit_image_button_action(_:)), for: .touchUpInside)
        username_text_field.setBottomBorder(withColor: UIColor.lightGray)
        useremail_text_field.setBottomBorder(withColor: UIColor.lightGray)
        usertype_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_mobile_text_field.setBottomBorder(withColor: UIColor.lightGray)
        usertype_text_field.rightView = self.btnDropDown
        usertype_text_field.rightViewMode = .always
        usertype_text_field.isUserInteractionEnabled = false
        usertype_text_field.rightView?.isUserInteractionEnabled = false
        user_type_button.isHidden = false
        user_type_button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        
        username_text_field.delegate = self
        useremail_text_field.delegate = self
        user_mobile_text_field.delegate = self
     
        usertype_text_field.delegate = self
  
      
//        userstatus_text_field.rightView = self.switch_button
//        usertype_text_field.rightViewMode = .always
        
        
    }
   
    @objc
    func updateProfile_API(_ sender : UIButton) {
        
        
        if username_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Name")
        }
        if useremail_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Email")
            
        }
        if usertype_text_field.text == "" {
            
            self.displayAlert(msg: "Enter user_type")
        }
            
      
            
        else {
            
            print(username_text_field.text as Any)
            if imagePath != "" {
                
                let update_API = ("\(APINAME.init().UPDATE_PROFILE_API)/\(String(describing: profile_dict.object(forKey: "id")!))")
                
                if usertype_text_field.text == "Admin" {
                    
                    user_type = 1
                }
                if usertype_text_field.text == "HR Manager" {
                    
                    user_type = 2
                }
                if usertype_text_field.text == "Recruiter" {
                    
                    user_type = 3
                }
                
                if status_Label.text == "Enabled" {
                    
                    user_status = 1
                }
                
                if status_Label.text == "Disabled" {
                    
                    user_status = 0
                }
                
                
                
                let params = ["name" : username_text_field.text! ,"mobile" : user_mobile_text_field.text!, "email" : useremail_text_field.text! , "user_type" : user_type, "status" :  user_status , "profile_pic" : imagePath] as [String : Any]
                
                print(update_API)
                print(imagePath)
                
                WebService.requestPUTUrlWithJSONDictionaryParameters(strURL: update_API, is_loader_required: true, params: params, success: { (response) in
                    print(response)
                    if response["status_code"] as! NSNumber == 1 {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else {
                        
                        self.displayAlert(msg: response["message"] as! String)
                        
                    }
                }) { (error) in
                    
                    print(error)
                }
                
                
            }
                
            else {
                
                displayAlert(msg: "Please select image")
                
            }
            
        }
        
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return components.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return components[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        usertype_text_field.text = components[row]
        
        self.view.endEditing(true)
    }
    
    internal var btnDropDown: UIButton {
        let size: CGFloat = 20.0
        
        button.setImage(UIImage(named: "drop_down"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    
//    internal var switch_button: UISwitch {
//        let size: CGFloat = 20.0
//
//        self.switch_button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
//        self.switch_button.addTarget(self, action: #selector(switch_button_action(_:)), for: .touchUpInside)
//        return self.switch_button
//    }
    

    @objc func YOUR_BUTTON__TAP_ACTION(_ sender: UIButton) {
        picker = UIPickerView.init()
        picker.delegate = self
        //   picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.setValue(UIColor.white, forKey: "backgroundColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    
    @objc func onDoneButtonTapped() {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
    }

}

extension UpdateProfileViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print(newString)
        
        if textField.placeholder == "Name" {
            
            let userInput = newString
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            if userInput.rangeOfCharacter(from: set.inverted) != nil {
                print("ERROR: There are numbers included!")
                displayAlert(msg: "Name requires alphabets only.")
                
            }
            else {
                
                print(newString)
            }
        }
        
        return true
        
        
    }
    
    
}

extension UpdateProfileViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
        self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
        // apiMultipart(image_data: user_image)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                            print(self.imagePath)
                            DispatchQueue.main.async {
                                let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                if image_url != nil
                                {
                                    if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                    {
                                        self.userImage = UIImage(data: data1 as Data)
                                    }
                                    
                                }
                                
                                SVProgressHUD.dismiss()
                                self.reloadInputViews()
                                //     self.tableView.reloadData()
                            }
                            
                            print(JSON)
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
}
