//
//  ViewController.swift
//  Recruitment
//
//  Created by Apple on 04/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate{
  

    @IBOutlet weak var login_Btn: UIButton!
    @IBOutlet weak var main_logo_img: UIImageView!
    
    
    @IBOutlet weak var emailOrMobile_textfield: UITextField!
    
    @IBOutlet weak var password_textfield: UITextField!
    
    @IBOutlet weak var user_type_textfield: UITextField!
    
      let userDefaults = UserDefaults.standard
    var window : UIWindow?
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    let button = UIButton(type: .custom)
    let show_password_button = UIButton(type: .custom)
    let hide_password_button = UIButton(type: .custom)
    
    var components = ["Admin","Manager","Recruiter"]
    
    @IBAction func forgot_password_action(_ sender: UIButton) {
        
        let forgot_Password_VC = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
        
        self.navigationController?.pushViewController(forgot_Password_VC, animated: true)
    }
    
    
    @IBAction func login_button_action(_ sender: UIButton) {

//        let home_VC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//
//        self.navigationController?.pushViewController(home_VC, animated: true)
        
        if emailOrMobile_textfield.text?.isEmpty == true {
            displayAlert(msg: "Enter your email first")
        }
        else if password_textfield.text?.isEmpty == true {
            
            displayAlert(msg: "Enter your password first")
        }
        else {
            
       call_login_API()
            
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.login_Btn.layer.masksToBounds = true
        self.login_Btn.layer.cornerRadius = 3
        
        picker.delegate = self
//
//        user_type_textfield.rightView = self.btnDropDown
//        user_type_textfield.rightViewMode = .always
        password_textfield.rightView = self.btnPasswordHide
        password_textfield.rightViewMode = .always
        
        
        password_textfield.isSecureTextEntry = true
        emailOrMobile_textfield.setBottomBorder(withColor: UIColor.lightGray)
        password_textfield.setBottomBorder(withColor: UIColor.lightGray)
//        user_type_textfield.setBottomBorder(withColor: UIColor.lightGray)
    }
    
    
    
    
    override func viewDidLayoutSubviews() {
//        user_type_textfield.delegate = self
        emailOrMobile_textfield.delegate = self
        password_textfield.delegate = self
        
//        user_type_textfield.rightView = self.btnDropDown
//        user_type_textfield.rightViewMode = .always
        password_textfield.rightView = self.btnPasswordHide
        password_textfield.rightViewMode = .always
        
        
        password_textfield.isSecureTextEntry = true
        emailOrMobile_textfield.setBottomBorder(withColor: UIColor.lightGray)
        password_textfield.setBottomBorder(withColor: UIColor.lightGray)
//        user_type_textfield.setBottomBorder(withColor: UIColor.lightGray)
        
    }
    
    func call_Access_Token_API () {
        
     let ACCESS_TOKEN_API = APINAME.init().ACCESS_TOKEN_API
        let params : NSDictionary! = ["username" : emailOrMobile_textfield.text! , "password" : password_textfield.text! , "client_secret" : "l35jki7SUgFYS071UWtXIPFKxGAYDULQ9Yfd7UXm" , "client_id" : "2" , "grant_type" : "password"]

        
        print(params)
        print(ACCESS_TOKEN_API)
        WebService.requestPostUrl(strURL: ACCESS_TOKEN_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in

                
                print(response)
                
                access_token = (response["access_token"] as! String)
                refresh_token = (response["refresh_token"] as! String)
                token_type  = (response["token_type"] as! String)
                let expireTime = (response["expires_in"] as! NSNumber)
                // let expireDate = Date().addingTimeInterval(TimeInterval(exactly: 60)!)
                let expireDate = Date().addingTimeInterval(TimeInterval(exactly: expireTime)!)
                self.userDefaults.setValue(expireDate, forKey: "expireDate")
                self.userDefaults.setValue(access_token, forKey: "access_token")
                self.userDefaults.setValue(refresh_token, forKey: "refresh_token")
                self.userDefaults.setValue(token_type, forKey: "token_type")
                
                
                let homeVC = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let nav = UINavigationController(rootViewController: homeVC)
                
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
                
         

        }) { (error) in

            print(error)
        }
        
       
        
        
    }
    
    func call_login_API() {
        let LOGIN_API = APINAME.init().LOGIN_API
        let params : NSDictionary! = ["email" : emailOrMobile_textfield.text! , "password" : password_textfield.text!]
        
    
        WebService.requestPostUrl(strURL: LOGIN_API, params: params, is_loader_required: true, success: { (response) in
            
            print(response)
            
            if response["status_code"] as! Int == 1 {
            print(response)
       
               self.call_Access_Token_API()
            
                
                let user_data = UserdataClass(User_Email_id: (((response["data"] as! [NSDictionary])[0] )["email"] as! String), User_id: (((response["data"] as! [NSDictionary])[0] )["id"] as! Int), User_type: (((response["data"] as! [NSDictionary])[0] )["user_type"] as! Int) , User_name: (((response["data"] as! [NSDictionary])[0] )["name"] as! String) , User_pic: (((response["data"] as! [NSDictionary])[0] )["profile_pic"] as! String))
                
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user_data)
                self.userDefaults.set(encodedData, forKey: "user_data")
                self.userDefaults.synchronize()
                
            }
            
            else {
                
                self.displayAlert(msg: response["message"] as! String)
                print("nhi nhi")
            }
        }) { (error) in
            
            print(error)
        }
        
        
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
   
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return components.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return components[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
//           user_type_textfield.text = components[row]
        
        self.view.endEditing(true)
    }

    
    @objc func YOUR_BUTTON__TAP_ACTION(_ sender: UIButton) {
        picker = UIPickerView.init()
        picker.delegate = self
     //   picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.setValue(UIColor.white, forKey: "backgroundColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    
    @objc func onDoneButtonTapped() {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
    }
    
  
    
    internal var btnDropDown: UIButton {
        let size: CGFloat = 20.0
       
        button.setImage(UIImage(named: "drop_down"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    internal var btnPasswordShow: UIButton {
        let size: CGFloat = 20.0
      
        show_password_button.setImage(UIImage(named: "show_password"), for: .normal)
        show_password_button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        show_password_button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        show_password_button.addTarget(self, action: #selector(hide_password_button_action(_:)), for: .touchUpInside)
        return show_password_button
    }
    internal var btnPasswordHide: UIButton {
        let size: CGFloat = 20.0
      
        hide_password_button.setImage(UIImage(named: "hide_password"), for: .normal)
        hide_password_button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        hide_password_button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        hide_password_button.addTarget(self, action: #selector(show_password_button_action(_:)), for: .touchUpInside)
        return hide_password_button
    }
    
    @objc func show_password_button_action(_ sender : UIButton) {
        
        hide_password_button.isHidden = true
        show_password_button.isHidden = false
        password_textfield.rightView = self.btnPasswordShow
        password_textfield.rightViewMode = .always
        password_textfield.isSecureTextEntry = false
        
    }
    @objc func hide_password_button_action(_ sender : UIButton) {
        
        hide_password_button.isHidden = false
        show_password_button.isHidden = true
        password_textfield.rightView = self.btnPasswordHide
        password_textfield.rightViewMode = .always
        password_textfield.isSecureTextEntry = true
        
        
    }
    
   

}

extension ViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        textField.returnKeyType = .done
        return true
    }
    
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
        
    }
    
    
}

