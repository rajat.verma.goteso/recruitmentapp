//
//  ChangePasswordViewController.swift
//  Recruitment
//
//  Created by Apple on 18/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePasswordViewController: UIViewController {

    
    @IBOutlet weak var old_password_text_field: UITextField!
    
    @IBOutlet weak var confirm_new_password_text_field: UITextField!
    @IBOutlet weak var new_password_text_field: UITextField!
    
    @IBOutlet weak var change_password_button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Change Password"
        addBackButton()
        
        old_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
         new_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
         confirm_new_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
        
        change_password_button.layer.masksToBounds = true
        change_password_button.layer.cornerRadius = 3
        change_password_button.addTarget(self, action: #selector(change_password_API), for: .touchUpInside)
    }
    
    @objc
    func change_password_API() {
        
        
       
        let change_Password_API = APINAME.init().CHANGE_PASSWORD_API_
        
        if old_password_text_field.text?.isEmpty == true{
            
            displayAlert(msg: "Enter Old Password")
            
            
        }
        if new_password_text_field.text?.isEmpty == true {
            displayAlert(msg: "Enter New Password")
        }
        
        if confirm_new_password_text_field.text?.isEmpty == true {
            
             displayAlert(msg: "Confirm New Password")
        }
        
        else {
        
        let params = ["old_password" : old_password_text_field.text! , "new_password" : new_password_text_field.text!, "new_password_confirmation" : confirm_new_password_text_field.text!] as [String: Any]
        
            print(params)
            print(change_Password_API)
      
            WebService.requestPostUrl(strURL: change_Password_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
                
                print(response)
                
                if response["status_code"] as! NSNumber == 1 {
                    
                    self.PasswordChangedisplayAlert(msg: response["message"] as! String)
                    
                }
                    
                else {
                    
                    self.displayAlert(msg: response["message"] as! String)
                }
                
                
                
            }) { (error) in
                
                print(error )
                
            }
            
        }
        
        
    }
    
    func displayAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func PasswordChangedisplayAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            self.navigationController?.popViewController(animated: true)
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
  

}
