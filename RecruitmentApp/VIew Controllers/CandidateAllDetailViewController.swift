//
//  CandidateAllDetailViewController.swift
//  Recruitment
//
//  Created by Apple on 12/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class CandidateAllDetailViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
  
  
    @IBOutlet weak var add_button: UIButton!
    @IBOutlet weak var user_image: UIImageView!
    @IBOutlet weak var main_view: UIView!
    
    @IBOutlet weak var candidate_list_table_view: UITableView!
    
    @IBOutlet weak var edit_button: UIButton!
    
    @IBOutlet weak var delete_button: UIButton!
    
    var candidate_list_dict = NSDictionary ()
     var new_candidate_list_dict = NSDictionary ()
    var candidate_list_array = [NSDictionary] ()
    var candidate_place_holder_array = [String] ()
    var imagePath = ""
    
    var window: UIWindow?
    
    var id : NSNumber!
    var designation : NSNumber!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        user_image.layer.masksToBounds = true
        user_image.layer.cornerRadius = 60
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Candidate Information"
       
        addBackButton()
       
        edit_button.addTarget(self, action: #selector(edit_button_action(_:)), for: .touchUpInside)
        delete_button.addTarget(self, action: #selector(displayWarningAlert(_:)), for: .touchUpInside)
        candidate_list_table_view.register(UINib(nibName: "CandidateAllDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "CandidateAllDetailsTableViewCell")
        get_Candidate_API()
        
    }
    
        func delete_button_action() {
        
      let delete_candidate_API = ("\(APINAME.init().Delete_CANDIDATE_API)/\(id!)")
        
        print(delete_candidate_API)
        WebService.requestDelUrl(strURL: delete_candidate_API, is_loader_required: true, params: [:], success: { (response) in
            
            print(response)
            
            if response["status_code"] as! NSNumber == 1 {
                
                self.displayDeleteAlert(msg: response["message"] as! String)
                
            }
            else {
                self.displayAlert(msg:  response["message"] as! String)
                
            }
           
            
        }) { (error) in
            
            print(error)
            
        }
        
        
        
    }
    
    @objc func edit_button_action(_ sender : UIButton) {
        
        let update_candidate_VC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateCandidateViewController") as! UpdateCandidateViewController
        
        update_candidate_VC.id = id
        update_candidate_VC.designation = designation!
        update_candidate_VC.candidate_list_array = candidate_list_array
        update_candidate_VC.imagePath = imagePath
        self.navigationController?.pushViewController(update_candidate_VC, animated: true)
        
    }
    func displayAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
          
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayDeleteAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
//
//            self.get_Candidate_API()
             self.navigationController?.popViewController(animated: true)
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
  
    func get_Candidate_API()  {
        
        let get_candidate_API = ("\(APINAME.init().GET_CANDIDATE_API)?id=\(id!)")
        
        print(get_candidate_API)
        
        WebService.requestGetUrlWithoutParameters(strURL: get_candidate_API, is_loader_required: true, success: { (response) in
            
            print(response)
            
            if response["status_code"] as! NSNumber == 1 {
                
                self.candidate_list_dict = response["data"] as! NSDictionary
                
                print(self.candidate_list_dict)
                if self.candidate_list_dict.count > 0 {
                    
                    
                    if (self.candidate_list_dict.object(forKey: "profile_pic") as! String).isEmpty == true {
                        
                        print("image hai ni")
                        
                    }
                    else{
                        
                        if self.imagePath.isEmpty
                        {
                            self.imagePath = (self.candidate_list_dict.object(forKey: "profile_pic") as! String)
                        }
                        let image_url = URL(string: BASE_IMAGE_URL + self.imagePath)
                        print(image_url as Any)
                        
                        self.user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
                        
                        
                    }
                    
                }
                
                self.candidate_place_holder_array = ["Name", "Email" , "Date_of_birth" , "Hometown", "Current_address", "Mobile" , "Whatsapp" ,"Experience" ,"Marital_status" , "Gender" , "Current_salary", "Expected_salary" , "Interview_date_time" , "Notice_period" , "Linkedin", "Instagram" , "Twitter" , "Facebook" , "Behance" , "Github" , "Stackoverflow" , "Youtube" ]
                
                
              //  self.candidate_list_array.append(["profile_pic" : self.candidate_list_dict.object(forKey: "profile_pic") as! String])
               
                
             
                if let dob = self.candidate_list_dict.object(forKey: "date_of_birth") as? String{
                    
                    self.candidate_list_array.append(["date_of_birth" : dob])
                }
                else
                {
                    self.candidate_list_array.append(["date_of_birth" : ""])
                }
                if  let inter_view_date_time = self.candidate_list_dict.object(forKey: "interview_date_time") as? String{
                    
                    self.candidate_list_array.append(["interview_date_time" : inter_view_date_time])
                }
                else
                {
                    self.candidate_list_array.append(["interview_date_time" : ""])
                }
                if let name = self.candidate_list_dict.object(forKey: "name") as? String{
                    
                   self.candidate_list_array.append(["name" : name])
                }
                else
                {
                    self.candidate_list_array.append(["name" : ""])
                }
                if  let email = self.candidate_list_dict.object(forKey: "email") as? String{
                    
                      self.candidate_list_array.append(["email" : email])
                }
                else
                {
                    self.candidate_list_array.append(["email" : ""])
                }
              
                if  let hometown = self.candidate_list_dict.object(forKey: "hometown") as? String{
                    
                    self.candidate_list_array.append(["hometown" : hometown])
                }
                else
                {
                    self.candidate_list_array.append(["hometown" : ""])
                }
                
                
                if let current_salary = self.candidate_list_dict.object(forKey: "current_address") as? String{
                    
                     self.candidate_list_array.append(["current_address" : current_salary])
                }
                else
                {
                    self.candidate_list_array.append(["current_address" : ""])
                }
                if let mobile = self.candidate_list_dict.object(forKey: "mobile") as? String{
                    
                     self.candidate_list_array.append(["mobile" : mobile])
                }
                else
                {
                    self.candidate_list_array.append(["mobile" : ""])
                }
                if let whatsapp = self.candidate_list_dict.object(forKey: "whatsapp") as? NSNumber{
                    
                   self.candidate_list_array.append(["whatsapp" : whatsapp])
                }
                else
                {
                    self.candidate_list_array.append(["whatsapp" : ""])
                }
                if let marital_status = self.candidate_list_dict.object(forKey: "marital_status") as? String{
                    
                     self.candidate_list_array.append(["marital_status" : marital_status])
                }
                else
                {
                    self.candidate_list_array.append(["marital_status" : ""])
                }
                if let gender = self.candidate_list_dict.object(forKey: "gender") as? String{
                    
                   self.candidate_list_array.append(["gender" : gender])
                }
                else
                {
                    self.candidate_list_array.append(["gender" : ""])
                }
                if let current_salary = self.candidate_list_dict.object(forKey: "current_salary") as? Int{
                    
                    self.candidate_list_array.append(["current_salary" : current_salary])
                }
                else
                {
                    self.candidate_list_array.append(["current_salary" : 0])
                }
                if let expected_salary = self.candidate_list_dict.object(forKey: "expected_salary") as? Int{
                    
                     self.candidate_list_array.append(["expected_salary" : expected_salary])
                }
                else
                {
                    self.candidate_list_array.append(["expected_salary" : 0])
                }
             
                if let notice_period =  self.candidate_list_dict.object(forKey: "notice_period") as? String{
                    
                  self.candidate_list_array.append(["notice_period" : notice_period])
                }
                else
                {
                    self.candidate_list_array.append(["notice_period" : ""])
                }
                if let linkedin = self.candidate_list_dict.object(forKey: "linkedin") as? String{
                    
                  self.candidate_list_array.append(["linkedin" : linkedin])
                }
                else
                {
                    self.candidate_list_array.append(["linkedin" : ""])
                }
                if let instagram =  self.candidate_list_dict.object(forKey: "instagram") as? String{
                    
                    self.candidate_list_array.append(["instagram" : instagram])
                }
                else
                {
                    self.candidate_list_array.append(["instagram" : ""])
                }
                if let twitter = self.candidate_list_dict.object(forKey: "twitter") as? String{
                    
                  self.candidate_list_array.append(["twitter" : twitter])
                }
                else
                {
                    self.candidate_list_array.append(["twitter" : ""])
                }
                if let facebook = self.candidate_list_dict.object(forKey: "facebook") as? String{
                    
                    self.candidate_list_array.append(["facebook" : facebook])
                
                }
                else
                {
                    self.candidate_list_array.append(["facebook" : ""])
                }
                
                if let behance = self.candidate_list_dict.object(forKey: "behance") as? String{
                    
                   self.candidate_list_array.append(["behance" : behance])
                }
                else
                {
                    self.candidate_list_array.append(["behance" : ""])
                }
                if let github = self.candidate_list_dict.object(forKey: "github") as? String{
                    
                  self.candidate_list_array.append(["github" : github])
                }
                else
                {
                    self.candidate_list_array.append(["github" : ""])
                }
                if let stack = self.candidate_list_dict.object(forKey: "stackoverflow") as?String{
                    
                     self.candidate_list_array.append(["stackoverflow" : stack])
                }
                else
                {
                    self.candidate_list_array.append(["stackoverflow" : ""])
                }
                if let youtube = self.candidate_list_dict.object(forKey: "youtube") as? String{
                    
                   self.candidate_list_array.append(["youtube" : youtube])
                }
                else
                {
                    self.candidate_list_array.append(["youtube" : ""])
                }
                
                if let experience = self.candidate_list_dict.object(forKey: "experience") as? Int{
                    
                    self.candidate_list_array.append(["experience" : experience])
                }
                else
                {
                    self.candidate_list_array.append(["experience" : 0])
                }
                
                print(self.candidate_list_array)
                
                DispatchQueue.main.async {
                    
                    self.candidate_list_table_view.reloadData()
                    
                }
                
            }
            
            
        }) { (error) in
            
            print(error)
            
        }
        
    }
    
    
    @objc func displayWarningAlert(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.candidate_list_table_view)
        let indexPath = self.candidate_list_table_view.indexPathForRow(at: point)
        let cell = self.candidate_list_table_view.cellForRow(at: indexPath!) as! CandidateAllDetailsTableViewCell
        
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: "Are you sure?" , preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "delete", style: .default, handler: { (action) in
            
            self.delete_button_action()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action) in
            
            return
        }))
        
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return candidate_place_holder_array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(candidate_list_array)
       
        let cell = candidate_list_table_view.dequeueReusableCell(withIdentifier: "CandidateAllDetailsTableViewCell") as! CandidateAllDetailsTableViewCell
        
        cell.selectionStyle = .none
        print(candidate_place_holder_array)
        cell.text_field.placeholder = (candidate_place_holder_array[indexPath.row] )
       
       
        if candidate_list_array.count > 0 {
            
            if cell.text_field.placeholder == "Name" {
                
                cell.label.text = "Name :"
                for item in candidate_list_array {
                    
                     let data = item
                    
                    if let val = data["name"] {
                        
                        print(val)
                       // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "designation")
                        
                    }
                    
                }
               
           
             
                
            }
            if cell.text_field.placeholder == "Email" {
                
                cell.label.text = "Email :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["email"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "email")
                        
                    }
                    
                }
                
                

               

            }


            if cell.text_field.placeholder == "Date_of_birth" {
                
                cell.label.text = "Date Of Birth :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["date_of_birth"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                      
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "dob")

                        
                    }
                    
                }

            }


            if cell.text_field.placeholder == "Hometown" {

                cell.label.text = "HomeTown :"
                for  item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["hometown"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "hometown")

                        
                    }
                    
                }
              
            }

            if cell.text_field.placeholder == "Current_address" {

                cell.label.text = "Current Address :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["current_address"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "location")
                        
                    }
                    
                }
               

            }

            if cell.text_field.placeholder == "Mobile" {

                cell.label.text = "Mobile Number :"
                // cell.text_field.text = (self.candidate_list_dict.object(forKey: "date_of_birth") as! String)
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["mobile"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                       
                        cell.text_field.text = val as? String
                           cell.image_icon.image = UIImage(named: "mobile_number")
                       
                        
                    }
                    
                }
                
               
            }


            if cell.text_field.placeholder == "Whatsapp" {

              
                cell.label.text = "WhatsApp :"
                // cell.text_field.text = (self.candidate_list_dict.object(forKey: "date_of_birth") as! String)
                
                // cell.text_field.text = (self.candidate_list_dict.object(forKey: "date_of_birth") as! String)
                for  item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["whatsapp"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                       
                     cell.text_field.text = val as! String
                        cell.image_icon.image = UIImage(named: "whatsapp")
                       
                        
                    }
                    
                }
                
             

            }

            if cell.text_field.placeholder == "Marital_status" {

                cell.label.text = "Marital Status :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["marital_status"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "martial_status")
                    }
                    
                }
                
              

            }

            if cell.text_field.placeholder == "Gender" {

                cell.label.text = "Gender :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["gender"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "gender")

                    }
                    
                }
                
                
                
              
            }
            if cell.text_field.placeholder == "Current_salary" {

              
                cell.label.text = "Current Salary :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["current_salary"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        if val as! Int == 0 {
                            
                            cell.text_field.text = "Fresher"
                            cell.image_icon.image = UIImage(named: "current_salary")
                        }
                        else {
                        cell.text_field.text = ("< \(val).0 L.P.A")
                        cell.image_icon.image = UIImage(named: "current_salary")
                      
                        }
                    }
                    
                }

            }
            if cell.text_field.placeholder == "Expected_salary" {

                cell.label.text = "Expected Salary :"
                for item in candidate_list_array {
                    
                    let data = item
                    if let val = data["expected_salary"] {
                        
                        print(val)
                        if val as! Int == 0 {
                            
                            cell.text_field.text = ""
                            cell.image_icon.image = UIImage(named: "expected_salary")
                        }
                     
                        else {
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = ("< \(val).0 L.P.A")
                        cell.image_icon.image = UIImage(named: "expected_salary")
                        }
                    }
                 
                    
                }
                
              

            }
            if cell.text_field.placeholder == "Interview_date_time" {

                cell.label.text = "Interview Date&Time :"
                for item in candidate_list_array{
                    
                    let data = item
                    
                    if let val = data["interview_date_time"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "time_card")
                    }
                    
                }
                
              

            }

         
            if cell.text_field.placeholder == "Notice_period" {

                cell.label.text = "Notice Period :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["notice_period"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "notice_period")
                    }
                    
                }
                
             

            }
            if cell.text_field.placeholder == "Linkedin" {

                cell.label.text = "Linkedin :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["linkedin"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "linkedin")
                    }
                    
                }
                
              

            }
            if cell.text_field.placeholder == "Instagram" {

                cell.label.text = "Instagram :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["instagram"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "instagram")
                    }
                    
                }
                
             

            }

            if cell.text_field.placeholder == "Facebook" {

                cell.label.text = "Facebook :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["facebook"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "face_book")

                    }
                    
                }
                
               
            }
            if cell.text_field.placeholder == "Twitter" {

                cell.label.text = "Twitter :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["twitter"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "tweet")
                        
                    }
                    
                }
                
               

            }
            if cell.text_field.placeholder == "Behance" {

                cell.label.text = "Behance :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["behance"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "behance")
                        
                    }
                    
                }
         

            }
            if cell.text_field.placeholder == "Github" {

                cell.label.text = "Github :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["github"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "github")
                    }
                    
                }
              

            }
            if cell.text_field.placeholder == "Stackoverflow" {

                cell.label.text = "Stackoverflow :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["stackoverflow"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "stack_overflow")
                    }
                    
                }
                
           

            }
            if cell.text_field.placeholder == "Youtube" {

                cell.label.text = "Youtube :"
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["youtube"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        cell.text_field.text = val as? String
                        cell.image_icon.image = UIImage(named: "youtube")
                    }
                }
              
            }
            
            if cell.text_field.placeholder == "Experience" {
                
                cell.label.text = "Experience :"
                for item in candidate_list_array {
                    
                    let data = item
                    if let val = data["experience"] {
                        
                        print(val)
                        if val as! Int == 0 {
                            
                            cell.text_field.text = "Fresher"
                            cell.image_icon.image = UIImage(named: "experience")
                        }
                            
                        else {
                            
                            // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                             cell.image_icon.image = UIImage(named: "experience")
                            let value = val as! Int
                            let year = (value/12)
                            
                            let month = (value % 12)
                            
                            
                            if value < 12 {
                                
                                if month == 1 {
                                    
                                    cell.text_field.text = ("\(month) month")
                                    
                                }
                                
                                else {
                                cell.text_field.text = ("\(month) months")
                                
                                }
                            }
                           
                           else {
                                
                                if year == 1 && month == 0 {
                                    
                                    cell.text_field.text = ("\(year) year")
                                    
                                }
                            
                              else  if year == 1 && month == 1 {
                                    
                                      cell.text_field.text = ("\(year) year \(month) month")
                                    
                                }
                                    
                               else if year > 1 && month < 1 {
                                    
                                    cell.text_field.text = ("\(year) years")
                                    
                                }
                                
                                else {
                                    
                                      cell.text_field.text = ("\(year) years \(month) months")
                                    
                                }
       
                            }
                            
                           
                        }
                    }
                   
                }
                
            }
            
        }
        
        return cell
        
    }
    
  
    


}
