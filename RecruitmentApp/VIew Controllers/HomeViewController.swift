//
//  HomeViewController.swift
//  Recruitment
//
//  Created by Apple on 10/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


 var user_data:UserdataClass!

class HomeViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var add_button: UIButton!
    var numberOfTimeCell : CGFloat { if UIDevice.current.userInterfaceIdiom == .pad
    {   return 4    }
    else{        return 2 }
    }
    
    @IBOutlet weak var home_collection_view: UICollectionView!
      var designations_Array = [NSDictionary] ()
    var new_designations_array = [NSDictionary] ()
    var designation_dict = NSDictionary()
    
    
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        // addBackButton()
        self.navigationItem.title = "Home"
        
    }
    
   
    
  
    override func viewWillAppear(_ animated: Bool) {
        
        user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        self.navigationController?.isNavigationBarHidden = false
        // addBackButton()
        self.navigationItem.title = "Home"
        
      
            
            let button = UIButton(type: .custom)
            button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
            button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
            button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
            //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
            button.clipsToBounds = true
            let barButton = UIBarButtonItem(customView: button)
            self.navigationItem.rightBarButtonItem = barButton
            
      
        
        let button2 = UIButton(type: .custom)
        button2.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button2.addTarget(self, action: #selector(my_profile_button_action(sender:)), for: UIControl.Event.touchUpInside)
        button2.setBackgroundImage(UIImage(named: "my_profile"), for: .normal)
        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
        button2.clipsToBounds = true
        let barButton2 = UIBarButtonItem(customView: button2)
        self.navigationItem.leftBarButtonItem = barButton2
        
         home_collection_view.register(UINib(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
       
        call_designation_API()
    }
    
    func call_designation_API() {
        
        let designationList_API = APINAME.init().DESIGNATION_LIST_API
        
        print(designationList_API)
        WebService.requestGetUrlWithoutParameters(strURL: designationList_API, is_loader_required: true, success: { (response) in
            
            print(response)
            
            self.designations_Array = response["data"] as! [NSDictionary]
            
            
            DispatchQueue.main.async {
                
                self.home_collection_view.reloadData()
            }
            
        }) { (error) in
            
            
            print(error)
        }
        
        
        
    }
    
//    @objc
//    func add_button_action(_ sender : UIButton) {
//        
//        let add_record_VC = self.storyboard?.instantiateViewController(withIdentifier: "AddRecordViewController") as! AddRecordViewController
//        
//        self.navigationController?.pushViewController(add_record_VC, animated: true)
//        
//    }
    
    @objc
    func rightButtonAction(sender: Any) {
        
        displayAlert(msg: "Choose")
        
    }
    @objc func my_profile_button_action(sender: Any) {
        
//        let my_profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
//        self.navigationController?.pushViewController(my_profile_VC, animated: true)
        
    }
    
    
    func displayAlert(msg:String) {
        
        let alert = UIAlertController()
        //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        if user_data.User_Type == 1 {
            
            alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
                
                let designation_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
                
                self.navigationController?.pushViewController(designation_VC, animated: true)
                
                return
            }))
            
            alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
                let modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ModeOfContactViewController") as! ModeOfContactViewController
                
                self.navigationController?.pushViewController(modeOfContact_VC, animated: true)
                return
            }))
            
            alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
                
                let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                
                self.navigationController?.pushViewController(profile_VC, animated: true)
                return
            }))
            
            
        }
   
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return designations_Array.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //  let width = (self.view.frame.size.width - 60)/numberOfTimeCell
        let width =  (self.view.frame.size.width - 60)/numberOfTimeCell
        
        return CGSize(width: width, height: 170)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = home_collection_view.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        
        cell.designation_title.text = ((designations_Array[indexPath.row] )["name"] as! String)
    
        
//        cell.main_view.layer.masksToBounds = true
//        cell.main_view.layer.cornerRadius = 10
        
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let candidate_list_VC = self.storyboard?.instantiateViewController(withIdentifier: "CandidateListViewController") as! CandidateListViewController
        
        candidate_list_VC.designation_dict = (self.designations_Array[indexPath.row] )
       // candidate_list_VC.designation_title = (self.designations_Array[indexPath.row] )
        
        self.navigationController?.pushViewController(candidate_list_VC, animated: true)
        
    }
    
   
}
