//
//  VerifyOtpViewController.swift
//  Recruitment
//
//  Created by Apple on 12/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class VerifyOtpViewController: UIViewController {

    @IBOutlet weak var user_image: UIImageView!
    @IBOutlet weak var reset_password_button: UIButton!
    
    @IBOutlet weak var enter_otp_text_field: UITextField!
    
    @IBOutlet weak var change_password_view: UIView!
    
    @IBOutlet weak var change_password_button: UIButton!
    
    @IBOutlet weak var enter_new_password_text_field: UITextField!
    
    @IBOutlet weak var confirm_password_field: UITextField!
    
    var email = ""
      var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Change Password"
        addBackButton()
        
        change_password_view.layer.masksToBounds = true
        change_password_view.layer.cornerRadius = 3
        user_image.layer.masksToBounds = true
        user_image.layer.cornerRadius = 65
        change_password_button.layer.masksToBounds = true
        change_password_button.layer.cornerRadius = 3
        enter_new_password_text_field.delegate = self
        enter_otp_text_field.delegate = self
        confirm_password_field.delegate = self
        
        enter_otp_text_field.setBottomBorder(withColor: UIColor.lightGray)
        enter_new_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
        
        confirm_password_field.setBottomBorder(withColor: UIColor.lightGray)
    
        reset_password_button.addTarget(self, action: #selector(verify_otp_API(_:)), for: .touchUpInside)
        change_password_button.addTarget(self, action: #selector(change_password_API(_:)), for: .touchUpInside)
    }
    
    @objc
    func change_password_API(_ sender : UIButton) {
        
        if enter_new_password_text_field.text?.isEmpty == true {
            
            displayAlert(msg: "Enter New Password")
            
        }
        if confirm_password_field.text?.isEmpty == true {
            
            displayAlert(msg: "Confirm New Password")
            
        }
            
        else {
            
            
            if enter_new_password_text_field.text! != confirm_password_field.text {
                
                
           displayAlert(msg: "Password did'nt match")
                
            }
            
            else {
                
               
                let change_Password_API = APINAME.init().CHANGE_PASSWORD_API
                
                let params = ["email" : email, "OTP" : enter_otp_text_field.text! , "password" : confirm_password_field.text! , "password_confirmation" : enter_new_password_text_field.text!] as [String : Any]
                
                WebService.requestPUTUrlWithJSONDictionaryParameters(strURL: change_Password_API, is_loader_required: true, params: (params as NSDictionary) as! [String : Any], success: { (response) in
                    
                    print(response)
                    
                    if response["status_code"] as! NSNumber == 1 {
                        
                        
                        self.displayAlertAction(msg: "Password Changed Successfully")
                        
                        
                    }
                        
                    else{
                        
                        self.displayAlert(msg: response["message"] as! String)
                    }
                    
                }) { (error) in
                    
                    print(error)
                }
            }
            
        }
        
        
        
        
    }
    
    @objc
    func verify_otp_API(_ sender : UIButton) {
        if enter_otp_text_field.text?.isEmpty == true {
            
            displayAlert(msg: "Enter OTP")
            
        }
        
        else {
            
            
            let verify_Otp_API = APINAME.init().VERIFY_OTP_API
            
            let params = ["email" : email, "OTP" : enter_otp_text_field.text!] as [String : Any]
            
            WebService.requestPostUrl(strURL: verify_Otp_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
                
                print(response)
                
                if response["status_code"] as! NSNumber == 1 {
                    
                    self.change_password_view.isHidden = false
                    
                }
                
                else {
                    
                    self.displayAlert(msg: response["message"] as! String)
                    
                }
                
                
            }) { (error) in
                
                print(error)
            }
            
        }
    }
    

    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlertAction(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    

}

extension VerifyOtpViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        textField.returnKeyType = .done
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
        
    }
    
    
}
