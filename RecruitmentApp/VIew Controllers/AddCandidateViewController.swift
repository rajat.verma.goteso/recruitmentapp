//
//  AddRecordViewController.swift
//  Recruitment
//
//  Created by Apple on 04/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import MobileCoreServices



class AddCandidateViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UIPickerViewDataSource, UIPickerViewDelegate , UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate{
   
   
    var experience_value = ""
    var current_salary_value = ""
    var expected_salary_value = ""
    
    @IBOutlet weak var create_candidate_button: UIButton!
    @IBOutlet weak var user_image: UIImageView!
   let userDefaults = UserDefaults.standard
      var user_data:UserdataClass!
    var index : Int!
    
    private var datePicker : UIDatePicker?
    private var date_and_time_Picker : UIDatePicker?
    
    
    @IBOutlet weak var edit_img_button: UIButton!
    var data_dict = NSDictionary ()
    
//    var record_data_placeholder = [NSDictionary(dictionaryLiteral: ("placeholder","Name of Candidate"),("value","")), "Gender" , "Designation" , "DOB" , "Mobile Number" , "Current Location" , "Hometown" , "Email" , "Current Salary" , "Expected Salary" , "Martial Status" , "Notice period" , "Resume" , "Whatsapp" , "Experience" , "Interview_Date_Time" , "Mode_Of_Contact" , "Verify_Status" , "Comments" , "Other_Details" , "Linkedin" , "Facebook" , "Twitter" , "Instagram" , "Github" , "Stackoverflow" , "Behance" , "Youtube" , "Created_By" ]
    
    var new_array = [NSDictionary(dictionaryLiteral: ("placeholder","Name of Candidate"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Gender"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","DOB"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Mobile Number"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Current Location"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Hometown"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Email"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Current Salary"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Expected Salary"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Martial Status"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Notice period"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Resume"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Whatsapp"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Experience"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Interview_Date_Time"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Mode Of Contact"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Comments"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Other_Details"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Linkedin"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Facebook"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Twitter"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Instagram"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Github"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Stackoverflow"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Behance"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Youtube"),("value",""))]
    
    var modeOfContact_Number : Int!
    var Year = ""
    var Month = ""
    var experience_data : Int!
    
   
    var name = ""
    var gender = ""
    var designation = ""
    var date_of_birth = ""
    var mobile = ""
    var current_address = ""
    var email = ""
    var current_salary = ""
    var expected_salary = ""
   
    
    var marital_status = ""
    var notice_period = ""
    var profile_pic = ""
    var resume = ""
    var whatsapp = ""
    var experience = ""
    var interview_date_time = ""
    var mode_of_contact = ""
    var varify_status = ""
    var comments = ""
    var other_details = ""
    var linkedin = ""
    var facebook = ""
    var twitter = ""
    var instagram = ""
    var github = ""
    var stackoverflow = ""
    var behance = ""
    var youtube = ""
    var created_by = ""
    var hometown = ""
    
   
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var components = ["123","456","789"]
    var gender_components = ["Select","Male" , "Female"]
    var current_salary_components = ["Select","Fresher", "< 1.0 L.P.A", "< 2.0 L.P.A" , "< 3.0 L.P.A" , "< 4.0 L.P.A" , "< 5.0 L.P.A" , "< 10.0 L.P.A" , "< 15.0 L.P.A" , "< 20.0 L.P.A" , "< 30.0 L.P.A" , "< 50.0 L.P.A"]
     var expected_salary_components = ["Select","Fresher","< 1.0 L.P.A", "< 2.0 L.P.A" , "< 3.0 L.P.A" , "< 4.0 L.P.A" , "< 5.0 L.P.A" , "< 10.0 L.P.A" , "< 15.0 L.P.A" , "< 20.0 L.P.A" ,  "< 30.0 L.P.A" , "< 50.0 L.P.A"]
    var martial_components = ["Select","Single/Unmarried", "Married" , "Widowed" , "Divorced" , "Separated" , "In-a-relationship"]
    var notice_components = ["Select","Tomorrow" , "Day after Tomorrow" , "After 3 days" , "After 5 days" , "After 1 week"]
    var verify_components = ["Select","Verified", "Unverified"]
    var experience_components_one = ["Select","Fresher", "1 year" , "2 years" , "3 years" , "4 years" , "5 years" , "6 years" , "7 years" , "8 years" , "9 years" , "10 years"]
     var experience_components_two = [ "Select","1 month", "2 months" , "3 months" , "4 months" , "5 months" , "6 months", "7 months" , "8 months" , "9 months" , "10 months" , "11 months"]
    var modeOfContact_components = [NSDictionary] ()
    var modeOfContact_Array = [NSDictionary] ()
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
    var image_url = ""
    var picker_type = ""
    
 @IBOutlet weak var Add_Record_Table_VIew: UITableView!
    
  
    @IBAction func Save_Btn_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        call_mode_of_contact_Api()
    }
    
    
    func date_picker_action() {
        
        datePicker = UIDatePicker ()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(AddCandidateViewController.dateChanged(datePicker:)), for: .valueChanged)
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AddCandidateViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        let calendar = Calendar.current
        
        datePicker?.setDate(Date(), animated: false)
        
        
        var eighteenYearsfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .year, value: -18, to: Date(), options: [])!
        }
        
        datePicker?.maximumDate = eighteenYearsfromNow
        datePicker?.setValue(UIColor.black, forKey: "textColor")
        datePicker?.setValue(UIColor.white, forKey: "backgroundColor")
        datePicker?.autoresizingMask = .flexibleWidth
        datePicker?.contentMode = .center
        datePicker!.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker!)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneDatePickerPressed))]
        self.view.addSubview(toolBar)
        
      
        
    }
    
    func date_and_time_picker_action() {
        
        
        date_and_time_Picker = UIDatePicker()
        date_and_time_Picker?.datePickerMode = .dateAndTime
        date_and_time_Picker?.addTarget(self, action: #selector(AddCandidateViewController.date_and_time_Changed(datePicker:)), for: .valueChanged)
        
        datePicker?.setDate(Date(), animated: false)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AddCandidateViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
        var sevenDaysfromNow: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: 1, to: Date(), options: [])!
        }
       
        date_and_time_Picker?.minimumDate = sevenDaysfromNow
        
      
        date_and_time_Picker?.setValue(UIColor.black, forKey: "textColor")
        date_and_time_Picker?.setValue(UIColor.white, forKey: "backgroundColor")
        date_and_time_Picker?.autoresizingMask = .flexibleWidth
        date_and_time_Picker?.contentMode = .center
        date_and_time_Picker!.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(date_and_time_Picker!)
        
        // datepicker toolbar setup
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(doneDatePickerPressed))]
        self.view.addSubview(toolBar)
        
    }
    
    
    @objc
    func doneDatePickerPressed(){
        
        toolBar.removeFromSuperview()
        datePicker?.removeFromSuperview()
        date_and_time_Picker?.removeFromSuperview()
        
    }
    

    
    @objc func viewTapped(gestureRecognizer : UITapGestureRecognizer) {
        
        
        view.endEditing(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        
        self.user_image.layer.masksToBounds = true
        self.user_image.layer.cornerRadius = 65
        self.Add_Record_Table_VIew.register(UINib(nibName: "AddRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "AddRecordTableViewCell")
        edit_img_button.layer.masksToBounds = true
        edit_img_button.layer.cornerRadius = 13
        edit_img_button.addTarget(self, action: #selector(edit_image_button_action(_:)), for: .touchUpInside)
        
        if userImage != nil
        {
            user_image.image = userImage
            user_image.contentMode = .scaleToFill
        }
        
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title =  "Add Candidate"
        addBackButton()
        
        create_candidate_button.addTarget(self, action: #selector(create_candidate_API(_:)), for: .touchUpInside)
        
        
    }
    
    
    internal var btnDropDown: UIButton {
        let size: CGFloat = 20.0
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "drop_down"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
     //   button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    internal var imgDropDown: UIImage {
        
        let size: CGFloat = 20.0
        
        let image = UIImage(named: "drop_down")
     
        return image!
    }
    
    internal var btnFilePicker: UIButton {
        let size: CGFloat = 20.0
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "filePicker"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
      //  button.addTarget(self, action: #selector(FILE_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if picker_type == "Experience" {
            
           return 2
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      
        if  picker_type == "Gender" {
            return gender_components.count
        }
        if picker_type == "Current Salary"  {
            
            
            return current_salary_components.count
        }
        if picker_type == "Expected Salary"  {
            
            
            return expected_salary_components.count
        }
        
        if picker_type == "Martial Status" {
            
            return martial_components.count
        }
        
        if picker_type == "Experience" {
            if component == 0 {
            
            return experience_components_one.count
                
            }
            else {
                
                return experience_components_two.count
                
            }
            
        }
        if picker_type == "Mode Of Contact" {
            
            return modeOfContact_components.count
        }
        
        return notice_components.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if  picker_type == "Gender" {
             return gender_components[row]
        }
        if picker_type == "Current Salary"  {
            
            
            return current_salary_components[row]
        }
        if picker_type == "Expected Salary"  {
            
            
            return expected_salary_components[row]
        }
        
        if picker_type == "Martial Status" {
            
            return martial_components[row]
        }
        
        if picker_type == "Experience" {
            
            if (component == 0) {
                
                 return experience_components_one[row]
                
            }
            else {
                return experience_components_two[row]
            }
            
           
            
        }
        
        if picker_type == "Notice period" {
            
            return notice_components[row]
        }
        if picker_type == "Mode Of Contact" {
            
            return ((modeOfContact_components[row])["title"] as! String)
        }
        
        return notice_components[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if picker_type == "Gender" {
            
            if gender_components[row] == "Select" {
                
                gender = ""
                
            }
            else {
                
                 gender = gender_components[row]
                
            }
           
        }
        if picker_type == "Current Salary" {
            
            if current_salary_components[row] == "Fresher" {
                
                current_salary = "0"
                
            }
            if current_salary_components[row] == "< 1.0 L.P.A" {
                
                current_salary = "1"
                
            }
            if current_salary_components[row] == "< 2.0 L.P.A" {
                
                current_salary = "2"
                print(current_salary)
                
            }
            if current_salary_components[row] == "< 3.0 L.P.A" {
                
                current_salary = "3"
                
            }
            if current_salary_components[row] == "< 4.0 L.P.A" {
                
                current_salary = "4"
                
            }
            if current_salary_components[row] == "< 5.0 L.P.A" {
                
                current_salary = "5"
                
            }
            if current_salary_components[row] == "< 10.0 L.P.A" {
                
                current_salary = "10"
                
            }
            if current_salary_components[row] == "< 15.0 L.P.A" {
                
                current_salary = "15"
                
            }
            if current_salary_components[row] == "< 20.0 L.P.A" {
                
                current_salary = "20"
                
            }
            if current_salary_components[row] == "< 30.0 L.P.A" {
                
                current_salary = "30"
                
            }
            if current_salary_components[row] == "< 50.0 L.P.A" {
                
                current_salary = "50"
                
            }
        
        }
        if picker_type == "Expected Salary" {
            
            if expected_salary_components[row] == "Fresher" {
                
                expected_salary = "0"
                
            }
            if expected_salary_components[row] == "< 1.0 L.P.A" {
                
                expected_salary = "1"
                
            }
            if expected_salary_components[row] == "< 2.0 L.P.A" {
                
                expected_salary = "2"
                
            }
            if expected_salary_components[row] == "< 3.0 L.P.A" {
                
                expected_salary = "3"
                
            }
            if expected_salary_components[row] == "< 4.0 L.P.A" {
                
                expected_salary = "4"
                
            }
            if expected_salary_components[row] == "< 5.0 L.P.A" {
                
                expected_salary = "5"
                
            }
            if expected_salary_components[row] == "< 10.0 L.P.A" {
                
                expected_salary = "10"
                
            }
            if expected_salary_components[row] == "< 15.0 L.P.A" {
                
                expected_salary = "15"
                
            }
            if expected_salary_components[row] == "< 20.0 L.P.A" {
                
                expected_salary = "20"
                
            }
            if expected_salary_components[row] == "< 30.0 L.P.A" {
                
                expected_salary = "30"
                
            }
            if expected_salary_components[row] == "< 50.0 L.P.A" {
                
                expected_salary = "50"
                
            }
            
        }
        if picker_type == "Experience" {
            
            if component == 0 {
                
                Year = experience_components_one[row]
                
                if Year == "Select" {
                    
                    Year = ""
                    
                }
                
                if Year == "Fresher" {
                    
                    Month = ""
                }
               
            }
                
            if component == 1 {
              
                print(Year)
                if Year == "Fresher"{
                    
                    Month = ""
                    displayAlert(msg: "You Selected Fresher")
                }
              
                else {
                
             Month = experience_components_two[row]
                    
                    if Month == "Select" {
                        
                        Month = ""
                    }
                    
                }
                
            }

            if Month == "" {
                
                experience = Year
                
               let year1 = calculate_experience(value: Year)
                print(year1)
                
                experience_data = (year1*12)
                print(experience_data!)
                
            }
            else if Year == "" {
                
                experience = Month
                 let month1 = calculate_experience(value: Month)
                print(month1)
                experience_data = (month1)
                print(experience_data!)
                
            }
            else {
            experience = ("\(Year) \(Month)")
                
                 let year1 = calculate_experience(value: Year)
                 let month1 = calculate_experience(value: Month)
                
                print(year1)
                print(month1)
                
                  experience_data = (year1*12) + (month1)
                print(experience_data!)
            
            }
            print(experience)
        
        }
        if picker_type == "Martial Status" {
            
            if martial_components[row] == "Select" {
                
                marital_status = ""
            }
            else {
                
                marital_status = martial_components[row]
            }
        }
        if picker_type == "Notice period" {
            
            if notice_components[row] == "Select" {
                
                notice_period = ""
                
            }
            
            else {
                
                notice_period = notice_components[row]
                
            }
          
        }
        if picker_type == "Mode Of Contact" {
            
            if ((modeOfContact_components[row] ) ["title"] as! String) == "Select" {
                
                mode_of_contact = ""
            }
                
            else {
            
            mode_of_contact = ((modeOfContact_components[row] ) ["title"] as! String)
            
            }
            
        }
        
        self.Add_Record_Table_VIew.reloadData()
        self.view.endEditing(true)
    }
    
    
    func calculate_experience(value : String) -> Int{
       
        
        if value == "Fresher"{
            
            return 0
        }
        
        if value == "1 year" || value == "1 month" {
            
            return 1
        }
        
        if value == "2 years" || value == "2 months" {
            
            return 2
        }
        if value == "3 years" || value == "3 months" {
            
            return 3
        }
        if value == "4 years" || value == "4 months" {
            
            return 4
        }
        if value == "5 years" || value == "5 months" {
            
            return 5
        }
       
        if value == "6 years" || value == "6 months" {
            
            return 6
        }
        if value == "7 years" || value == "7 months" {
            
            return 7
        }
        if value == "8 years" || value == "8 months" {
            
            return 8
        }
        if value == "9 years" || value == "9 months" {
            
            return 9
        }
        if value == "10 years" || value == "10 months" {
            
            return 10
        }
        
         if value == "11 months" {
            
            return 11
        }
        
         else{
            
            return 0
        }
       
    }
    
    
    @objc
    func create_candidate_API(_ sender : UIButton) {
        
        
         if mode_of_contact != "" {
            
            for item in modeOfContact_components {
                
                if mode_of_contact == item["title"] as! String {
                    
                    modeOfContact_Number = (item["id"] as! Int)
                    
                }
                
            }
            
        }
        if name == "" {
            
            displayAlert(msg: "Enter name")
            
        }
        

      if mobile == "" && email == "" && linkedin == "" && whatsapp == ""{

        displayAlert(msg: "Enter Any One from: Mobile , Email , Linkedin and WhatsApp")
        
        }
        
        
        else {
        
                let create_Candidate_API = APINAME.init().CREATE_CANDIDATE_API
      
        
        print(current_salary)
        print(expected_salary)
        print(experience_data)
        
        if experience_data == nil {
            
            experience_value = ""
        }
        
        else {
            
            experience_value = send_experience(value: experience_data)
            
        }
        
                let params = ["name" : name ,"email" : email , "mobile" : mobile ,"profile_pic" : imagePath, "gender" : gender , "resume" : "resume/1570881243.docx" , "marital_status" : marital_status , "date_of_birth" : date_of_birth , "whatsapp" : whatsapp , "hometown" : hometown, "current_address" : current_address , "designation"  : designation , "experience" : experience_value , "current_salary" : current_salary, "expected_salary" : expected_salary, "notice_period" : notice_period , "interview_date_time" : interview_date_time , "mode_of_contact" : modeOfContact_Number, "comments" : comments , "other_details" : other_details , "linkedin" : linkedin , "facebook" : facebook , "twitter" : twitter , "instagram" : instagram , "github" : github , "stackoverflow" : stackoverflow, "behance" : behance , "youtube" : youtube , "created_by" : user_data.User_ID!]  as [String : Any]
                
                print(params)
                
                print(imagePath)
                
                print(create_Candidate_API)
                WebService.requestPostUrl(strURL: create_Candidate_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
                    
                    print(response)
                    
                    if response["status_code"] as! NSNumber == 1 {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else {
                        
                        self.displayAlert(msg: response["message"] as! String)
                        
                    }
                    
                }) { (error) in
                    
                    
                    print(error)
                }
        
        }
    }
    
    func send_experience(value : Int) -> String {
        
        if value != nil {
            
          return ("\(value)")
            
        }
        
            return ""
       
    }
    
   
    
    func call_mode_of_contact_Api() {
        
        let mode_of_contact_API = APINAME.init().MODE_OF_CONTACT_API
        
        WebService.requestGetUrlWithoutParameters(strURL: mode_of_contact_API, is_loader_required: false, success: { (response) in
            
            print(response)
            
            self.modeOfContact_Array = response["data"] as! [NSDictionary]
            for item in self.modeOfContact_Array {
                
                self.modeOfContact_components.append(item )
                
            }
            print(self.modeOfContact_components)
            
            DispatchQueue.main.async {
                self.Add_Record_Table_VIew.reloadData()
            }
            
        }) { (error) in
            
            print(error)
            
        }
        
        
    }
    
  
    
//    @objc func select_gender_action (_ sender : UIButton) {
//
//       let index = sender.tag
//
//        print(new_array[index])
//        let center = sender.center
//        let point = sender.superview!.convert(center, to:self.Add_Record_Table_VIew)
//        let indexPath = self.Add_Record_Table_VIew.indexPathForRow(at: point)
//        let cell = self.Add_Record_Table_VIew.cellForRow(at: indexPath!) as! AddRecordTableViewCell
//
//        let alert = UIAlertController(title: "", message: "Select Gender", preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Male", style: .default, handler: { (action) in
//
//
//
//                self.gender = "Male"
//              cell.record_text_field.text = self.gender
//
//
//            self.Add_Record_Table_VIew.reloadData()
//            return
//        }))
//
//        alert.addAction(UIAlertAction(title: "Female", style: .default, handler: { (action) in
//
//                self.gender = "Female"
//                cell.record_text_field.text = self.gender
//
//                print(self.gender)
//
//             self.Add_Record_Table_VIew.reloadData()
//            return
//        }))
//
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
//
//
//            return
//        }))
//        let popOverController = alert.popoverPresentationController
//        popOverController?.sourceView = self.view
//        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//        popOverController?.permittedArrowDirections = []
//        self.present(alert, animated: true, completion: nil)
//
//
//
//    }
    
    @objc func YOUR_BUTTON__TAP_ACTION(_ sender: UIButton) {
        
        let index  = sender.tag
       
        print((new_array[index]))
        
        if (new_array[index])["placeholder"] as! String == "Resume" {
            
            file_picker_action()
            
        }
        else if (new_array[index])["placeholder"] as! String == "DOB" {
            
            date_picker_action()
            
        }
        
        else if (new_array[index])["placeholder"] as! String == "Interview_Date_Time" {
            
            date_and_time_picker_action()
            
        }
        
        else {
        
        if (new_array[index])["placeholder"] as! String == "Gender" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Current Salary" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Expected Salary" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Martial Status" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Notice period" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Experience" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Mode Of Contact" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
       
        
        picker = UIPickerView.init()
        picker.delegate = self
       // picker.backgroundColor = UIColor.lightGray
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.setValue(UIColor.white, forKey: "backgroundColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
        
        }
        
    }
    
    
    
    @objc func FILE_BUTTON__TAP_ACTION() {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
        
    }
    
    
    @objc func onDoneButtonTapped() {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    @objc func dateChanged(datePicker : UIDatePicker) {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
    
        
       date_of_birth = dateFormatter.string(from: datePicker.date)
   
        print(date_of_birth)
        
        self.Add_Record_Table_VIew.reloadData()
        view.endEditing(true)
        
    }
    
    @objc func date_and_time_Changed(datePicker : UIDatePicker) {
        
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        
        let dateformatter4 = DateFormatter()
        dateformatter4.dateFormat = "dd-MMMM-yyyy hh:mm a"
//        let dateString4 = dateformatter4.string(from: datePicker.date)
//        print("Date Selected \(dateString4)")
      interview_date_time = dateformatter4.string(from: datePicker.date)
        
        print(interview_date_time)
        
        self.Add_Record_Table_VIew.reloadData()
        view.endEditing(true)
        
    }
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return new_array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Add_Record_Table_VIew.dequeueReusableCell(withIdentifier: "AddRecordTableViewCell") as! AddRecordTableViewCell
        
        
        cell.tag = indexPath.row
        cell.record_text_field.tag = indexPath.row
        cell.selectionStyle = .none
        cell.record_text_field.setBottomBorder(withColor: UIColor.lightGray)
        cell.record_text_field.font = UIFont(name: "Open Sans", size: 12)
        cell.record_text_field.placeholder = ((new_array[indexPath.row] )["placeholder"] as! String)
        cell.record_text_field.delegate = self
        
        
        if cell.record_text_field.placeholder == "Name of Candidate" {
            
            cell.record_text_field.isUserInteractionEnabled = true
            cell.button.isHidden = true
            cell.record_text_field.keyboardType = .default
            cell.label.text = "Name :"
            
            print(cell.record_text_field.text!)
            print(name)
            
            if name == "" {

                cell.img_icon.image = UIImage(named: "designation")
                cell.record_text_field.rightView?.isHidden = true
                cell.record_text_field.text! = name
                print(name)

            }

            else {

                cell.img_icon.image = UIImage(named: "designation")
                cell.record_text_field.rightView?.isHidden = true
                cell.record_text_field.text! = name

            }
          
        }
        
        if cell.record_text_field.placeholder == "Gender" {
            
          //  cell.record_text_field.inputView = picker
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.img_icon.image = UIImage(named: "gender")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
             cell.label.text = "Gender :"
        
           
            if gender != "" {
                
             cell.record_text_field.text! = gender
                
            }
            
             cell.button.isHidden = false
             gender = cell.record_text_field.text!
           
        }
        
      
        if cell.record_text_field.placeholder == "DOB" {
            
         //   cell.record_text_field.inputView = picker
            cell.label.text = "Date Of Birth :"
            cell.img_icon.image = UIImage(named: "dob")
            cell.button.isHidden = false
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.record_text_field.keyboardType = .default
          
            
        if date_of_birth != "" {
            
          
            cell.record_text_field.text! = date_of_birth
            
        }
              date_of_birth = cell.record_text_field.text!
              cell.record_text_field.text! = date_of_birth
           
        
        }
        
        
        if cell.record_text_field.placeholder == "Mobile Number" {
            
           // cell.record_text_field.inputView = picker
            cell.button.isHidden = true
            cell.label.text = "Mobile Number :"
            cell.img_icon.image = UIImage(named: "mobile_number")
            cell.record_text_field.keyboardType = .phonePad
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
           
            if mobile != "" {
                
                 cell.record_text_field.text = mobile
                
            }
            
            mobile = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Current Location" {
            
             cell.label.text = "Current Location :"
            cell.img_icon.image = UIImage(named: "location")
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.isUserInteractionEnabled = true
            cell.button.isHidden = true
            
            if current_address != "" {
                
                 cell.record_text_field.text = current_address
                
            }
            
            current_address = cell.record_text_field.text!
            
        }
        
        
        if cell.record_text_field.placeholder == "Email" {
            
             cell.label.text = "Email :"
            cell.img_icon.image = UIImage(named: "email")
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.isUserInteractionEnabled = true
            cell.button.isHidden = true
            if email != "" {
                
                 cell.record_text_field.text = email
                
            }
            email = cell.record_text_field.text!
            
        }
        
        
        if cell.record_text_field.placeholder == "Hometown" {
            
            cell.label.text = "Hometown :"
            cell.img_icon.image = UIImage(named: "hometown")
            cell.button.isHidden = true
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.record_text_field.keyboardType = .default
           
            if hometown != "" {
                
                 cell.record_text_field.text = hometown
                
            }
            hometown = cell.record_text_field.text!
           
        }
        if cell.record_text_field.placeholder == "Current Salary" {
            
             cell.label.text = "Current Salary :"
            cell.img_icon.image = UIImage(named: "current_salary")
            cell.record_text_field.keyboardType = .numberPad
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
         
            cell.button.isHidden = false
           
            
            if current_salary != "" {
                
                if current_salary == "" {
                    print(current_salary)
                    cell.record_text_field.text = "Select"
                    
                }
                    
                if current_salary == "Fresher" {
                    print(current_salary)
                    cell.record_text_field.text = "Fresher"
                    
                }
                
                else {
                
                print(current_salary)
                cell.record_text_field.text = ("< \(current_salary).0 L.P.A")
            }
            }
            
           
        //    current_salary = Int(cell.record_text_field.text!)
           
        }
        
        if cell.record_text_field.placeholder == "Expected Salary" {
            
             cell.label.text = "Expected Salary :"
            cell.img_icon.image = UIImage(named: "expected_salary")
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
           
            cell.button.isHidden = false
          
            
            if expected_salary != "" {
                
                if expected_salary == "" {
                    print(expected_salary)
                    cell.record_text_field.text = "Select"
                    
                }
                
                if expected_salary == "Fresher" {
                    print(expected_salary)
                    cell.record_text_field.text = "Fresher"
                    
                }
                else {
                print(expected_salary)
                cell.record_text_field.text = ("< \(expected_salary).0 L.P.A")
                
            }
            }
           
           // expected_salary = Int(cell.record_text_field.text!)
            
            
        }
        
        if cell.record_text_field.placeholder == "Martial Status" {
            
             cell.label.text = "Martial Status :"
            cell.img_icon.image = UIImage(named: "martial_status")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
           
            if marital_status != "" {
                
                 cell.record_text_field.text = marital_status
            }
            
            marital_status = cell.record_text_field.text!
         
        }
        
        if cell.record_text_field.placeholder == "Notice period" {
            
             cell.button.isHidden = false
             cell.label.text = "Notice Period :"
            cell.img_icon.image = UIImage(named: "notice_period")
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
           
            if notice_period != "" {
                
                cell.record_text_field.text = notice_period
                
            }
            cell.button.isHidden = false
            cell.button.tag = indexPath.row
           
            notice_period = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Resume" {
            
            //  cell.record_text_field.inputView = picker
             cell.label.text = "Resume :"
            cell.img_icon.image = UIImage(named: "resume")
            cell.button.isHidden = false
            cell.record_text_field.rightView = self.btnFilePicker
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            
            if resume != "" {
                   cell.record_text_field.text = resume
                
            }
            resume = cell.record_text_field.text!
            
        }
            
        if cell.record_text_field.placeholder == "Whatsapp" {
            
             cell.label.text = "WhatsApp :"
            cell.img_icon.image = UIImage(named: "whatsapp")
            cell.record_text_field.keyboardType = .phonePad
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
            cell.button.isHidden = true
            if whatsapp != "" {
                
                cell.record_text_field.text = whatsapp
            }
            whatsapp = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Experience" {
            
            //   cell.record_text_field.inputView = picker
             cell.label.text = "Experience :"
            cell.img_icon.image = UIImage(named: "experience")
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            //     cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
            
            if experience != "" {
                 cell.record_text_field.text = experience
                
            }
            experience = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Interview_Date_Time" {
            
         // cell.record_text_field.inputView = picker
             cell.label.text = "Interview Date&Time :"
            cell.img_icon.image = UIImage(named: "time_card")
            cell.button.isHidden = false
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.record_text_field.isUserInteractionEnabled = false
            
            if interview_date_time != "" {
                
                cell.record_text_field.text = interview_date_time
            }
            interview_date_time = cell.record_text_field.text!
            
            
        }
            
         if cell.record_text_field.placeholder == "Mode Of Contact" {
            
             cell.label.text = "Mode Of Contact :"
            cell.img_icon.image = UIImage(named: "information")
            cell.record_text_field.rightView = self.btnDropDown
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
           
            if mode_of_contact != "" {
                  cell.record_text_field.text = mode_of_contact
                
            }
           mode_of_contact = cell.record_text_field.text!
            
        }
            
//         if cell.record_text_field.placeholder == "Verify_Status" {
//
//            cell.img_icon.image = UIImage(named: "email")
//            cell.record_text_field.rightView?.isHidden = false
//
//            if varify_status != "" {
//                 cell.record_text_field.text = varify_status
//
//            }
//
//            varify_status = cell.record_text_field.text!
//
//
//        }
        
         if cell.record_text_field.placeholder == "Comments" {
            
             cell.label.text = "Comments :"
            cell.img_icon.image = UIImage(named: "comments")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if comments != "" {
                 cell.record_text_field.text = comments
                
            }
            comments = cell.record_text_field.text!
            
            
        }
            
         if cell.record_text_field.placeholder == "Other_Details" {
            
             cell.label.text = "Other Details :"
            cell.img_icon.image = UIImage(named: "moreDetails")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if other_details != "" {
                
                 cell.record_text_field.text = other_details
                
            }
            other_details = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Linkedin" {
            
             cell.label.text = "Linkedin :"
            cell.img_icon.image = UIImage(named: "linkedin")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if linkedin != "" {
                
                  cell.record_text_field.text = linkedin
                
            }
            linkedin = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Facebook" {
            
             cell.label.text = "Facebook :"
            cell.img_icon.image = UIImage(named: "face_book")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if facebook != "" {
                
                cell.record_text_field.text = facebook
                
            }
            facebook = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Twitter" {
            
             cell.label.text = "Twitter :"
            cell.img_icon.image = UIImage(named: "tweet")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if twitter != "" {
                 cell.record_text_field.text = twitter
                
            }
            twitter = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Instagram" {
            
             cell.label.text = "Instagram :"
            cell.img_icon.image = UIImage(named: "instagram")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if instagram != "" {
                 cell.record_text_field.text = instagram
                
            }
            instagram = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Github" {
            
             cell.label.text = "Github :"
            cell.img_icon.image = UIImage(named: "github")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if github != "" {
                
                 cell.record_text_field.text = github
            }
            github = cell.record_text_field.text!
            
            
        }
            
        if cell.record_text_field.placeholder == "Stackoverflow" {
            
             cell.label.text = "Stackoverflow :"
            cell.img_icon.image = UIImage(named: "stack_overflow")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if stackoverflow != "" {
                
                cell.record_text_field.text = stackoverflow
                
            }
            stackoverflow = cell.record_text_field.text!
            
        }
         if cell.record_text_field.placeholder == "Behance" {
            
             cell.label.text = "Behance :"
            cell.img_icon.image = UIImage(named: "behance")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            if behance != "" {
                
                   cell.record_text_field.text = behance
                
            }
            behance = cell.record_text_field.text!
          
            
        }
            
         if cell.record_text_field.placeholder == "Youtube" {
            
             cell.label.text = "Youtube :"
            cell.img_icon.image = UIImage(named: "youtube")
            cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
            cell.button.isHidden = true
            
            if youtube != "" {
                
                  cell.record_text_field.text = youtube
                
            }
            youtube = cell.record_text_field.text!
           
        }
        
            cell.button.tag = indexPath.row
            cell.button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
     
        
        return cell
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    

    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
  
    func file_picker_action() {
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
   

}

extension AddCandidateViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds

        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
          let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        print(newString)
    
       let index = textField.tag
        
        print(index)
        
        
        if ((new_array[index] )["placeholder"] as! String) == "Name of Candidate" {

            name = newString
            print(name)
            
            
            let userInput = newString
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            if userInput.rangeOfCharacter(from: set.inverted) != nil {
                print("ERROR: There are numbers included!")
                displayAlert(msg: "Name requires alphabets only.")
                
            }
            
            else {
            
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
               data_dict.setValue(newString, forKey: "value")
                
                new_array[index] = data_dict
              
            }

            else {

             print(((new_array[index] )["value"] as! String))

            }
            }

        }

       
        if ((new_array[index] )["placeholder"] as! String) == "Mobile Number" {

            mobile = newString
            // cell.record_text_field.inputView = picker
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(newString, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
//            self.Add_Record_Table_VIew.reloadData()
        }

        if ((new_array[index] )["placeholder"] as! String) == "Current Location" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                print(newString)
                current_address = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
//            self.Add_Record_Table_VIew.reloadData()
        }

        if ((new_array[index] )["placeholder"] as! String) == "Email" {

            email = newString
            
            
            
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
          
            
//            self.Add_Record_Table_VIew.reloadData()
        }


        if ((new_array[index] )["placeholder"] as! String) == "Hometown" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                print(newString)
                hometown = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
        }

        if ((new_array[index] )["placeholder"] as! String) == "Resume" {

            resume = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
        }

        if ((new_array[index] )["placeholder"] as! String) == "Whatsapp" {

            whatsapp = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
          
        }


        if ((new_array[index] )["placeholder"] as! String) == "Interview_Date_Time" {

            interview_date_time = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
         
        }



        if ((new_array[index] )["placeholder"] as! String) == "Comments" {

            comments = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
       
        }

        if ((new_array[index] )["placeholder"] as! String) == "Other_Details" {


            other_details = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
        
        }

        if ((new_array[index] )["placeholder"] as! String) == "Linkedin" {

            linkedin = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
       
        }

        if ((new_array[index] )["placeholder"] as! String) == "Facebook" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                facebook = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
        }

        if ((new_array[index] )["placeholder"] as! String) == "Twitter" {

            twitter = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            

        }

        if ((new_array[index] )["placeholder"] as! String) == "Instagram" {

            instagram = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
 
        }

        if ((new_array[index] )["placeholder"] as! String) == "Github" {

            github = newString
            
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
 
        }

        if ((new_array[index] )["placeholder"] as! String) == "Stackoverflow" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                stackoverflow = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
    
        }
        if ((new_array[index] )["placeholder"] as! String) == "Behance" {

            behance = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
        
        }

        if ((new_array[index] )["placeholder"] as! String) == "Youtube" {

            youtube = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
     
        }
        return true
       
    }
    
   
    
}

extension AddCandidateViewController: UIImagePickerControllerDelegate {
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
      //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
           self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            if JSON["status_code"] as! NSNumber == 0 {
                                
                                self.displayAlert(msg: JSON["message"] as! String)
                                
                            }
                                
                            else {
                                
                                self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                                print(self.imagePath)
                                DispatchQueue.main.async {
                                    let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                    if image_url != nil
                                    {
                                        if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                        {
                                            self.userImage = UIImage(data: data1 as Data)
                                        }
                                        
                                    }
                                    
                                    SVProgressHUD.dismiss()
                                    self.reloadInputViews()
                                    //     self.tableView.reloadData()
                                }
                                
                                print(JSON)
                            }
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
    
    
    
}

