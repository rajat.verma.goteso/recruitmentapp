//
//  AddNewDesignationViewController.swift
//  Recruitment
//
//  Created by Apple on 10/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class AddNewDesignationViewController: UIViewController {

    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var add_button: UIButton!
    
    @IBOutlet weak var enter_designation_text_field: UITextField!
    
    @IBOutlet weak var user_image: UIImageView!
    
    
    @IBOutlet weak var edit_userImage_button: UIButton!
    var name = ""
    var image_url = ""
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
      
        edit_userImage_button.addTarget(self, action: #selector(edit_image_button_action), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        add_button.layer.masksToBounds = true
        add_button.layer.cornerRadius = 3
        enter_designation_text_field.setBottomBorder(withColor: UIColor.lightGray)
//        user_image.layer.masksToBounds = true
//        user_image.layer.cornerRadius = 50
//        user_image.clipsToBounds = true
//        user_image.contentMode = .scaleToFill
        main_view.layer.borderWidth = 1
//       edit_userImage_button.layer.masksToBounds = true
//        edit_userImage_button.layer.cornerRadius = 15
        
      //  add_button.addTarget(self, action: #selector(addNewDesignation_API(_:)), for: .touchUpInside)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Add Designation"
      
//        if userImage != nil
//        {
//            user_image.image = userImage
//            user_image.contentMode = .scaleToFill
//        }
        
          add_button.addTarget(self, action: #selector(addNewDesignation_API(_:)), for: .touchUpInside)
       
        
     //   addNewDesignation_API()
   }
    
   
    
//    @objc func add_button_target(_ sender : UIButton) {
//
//        self.navigationController?.popViewController(animated: true)
//
//    }
    
    @objc
    func addNewDesignation_API(_ sender : UIButton) {
        
        if enter_designation_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Designation")
        }
        else {
          
            print(enter_designation_text_field.text as Any)
//        if imagePath != "" {
            
            let new_designation_API = APINAME.init().ADD_NEW_DESIGNATION_API
            let params = ["name" : enter_designation_text_field.text!] as [String : Any]
            
            print(imagePath)
            
            WebService.requestPostUrl(strURL: new_designation_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
                
                print(response)
                
                if response["status_code"] as! NSNumber == 1 {
                
                self.navigationController?.popViewController(animated: true)
                    
                }
                else {
                    
                    self.displayAlert(msg: response["message"] as! String)
                    
                }
                
            }) { (error) in
                
                
                print(error)
            }
          
//        }
//
//        else {
//
//            displayAlert(msg: "Please select image")
//
//        }

        }

    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension AddNewDesignationViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
        self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
       // apiMultipart(image_data: user_image)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            print(JSON)
                            if JSON["status_code"] as! NSNumber == 0 {
                           
                                self.displayAlert(msg: JSON["message"] as! String)
                                
                                
                            }
                            
                            else {
                                
                               
                                self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                                print(self.imagePath)
                                DispatchQueue.main.async {
                                    let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                    if image_url != nil
                                    {
                                        if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                        {
                                            self.userImage = UIImage(data: data1 as Data)
                                        }
                                        
                                    }
                                    
                                    SVProgressHUD.dismiss()
                                    self.reloadInputViews()
                                    //     self.tableView.reloadData()
                                }
                                
                                print(JSON)
                                
                                
                            }
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
}
