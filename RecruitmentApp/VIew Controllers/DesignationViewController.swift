//
//  HomeViewController.swift
//  Recruitment
//
//  Created by Apple on 09/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON
import SVProgressHUD

class DesignationViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var add_button: UIButton!
    var numberOfTimeCell : CGFloat { if UIDevice.current.userInterfaceIdiom == .pad
    {   return 4    }
    else{        return 2 }
    }
    
    @IBOutlet weak var home_collection_view: UICollectionView!
    
    @IBOutlet weak var oops_image: UIImageView!
    var designations_Array = [NSDictionary] ()
    var designation_dict : NSDictionary!
     var imagePath = ""
    var designation_ID : NSNumber!
    
    var window : UIWindow?
    var user_data:UserdataClass!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        add_button.layer.masksToBounds = true
        add_button.layer.cornerRadius = 20
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Designation"
        home_collection_view.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
//
//        let button = UIButton(type: .custom)
//        button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
//        button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
//        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
//        button.clipsToBounds = true
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.rightBarButtonItem = barButton
        
    
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
           user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        
         call_designation_API()
        
        if user_data.User_Type != 1 {
            
            add_button.isHidden = true
        }
        
        self.add_button.addTarget(self, action: #selector(add_button_action(_:)), for: .touchUpInside)
        
//        if designations_Array.count == 0  {
//            
//            oops_image.isHidden = false
//            
//        }
//        
        
    }
    
    func call_designation_API() {
        
        let designationList_API = APINAME.init().DESIGNATION_LIST_API
        
        print(designationList_API)
        WebService.requestGetUrlWithoutParameters(strURL: designationList_API, is_loader_required: true, success: { (response) in
            
            print(response)
            
            if response["status_code"] as! NSNumber == 1 {
            
            self.designations_Array = response["data"] as! [NSDictionary]
            
              
            
            DispatchQueue.main.async {
                
                self.home_collection_view.reloadData()
            }
                
            }
            
            else{
               
                self.displayAlert(msg: response["message"] as! String)
                
            }
            
        }) { (error) in
            
            
            print(error)
        }
        
        
        
    }
    
    
    
    @objc
    
    func rightButtonAction(sender: Any) {
        
        displayAlert(msg: "Choose")
        
    }
    
    @objc func add_button_action(_ sender: UIButton) {
        
        
        let add_designation_VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewDesignationViewController") as! AddNewDesignationViewController
        
        
        self.navigationController?.pushViewController(add_designation_VC, animated: true)
        
    }
    
    func delete_designation_API(index_path : Int) {
        
        designation_ID = (designations_Array[index_path].object(forKey: "id") as! NSNumber)
        
        let delete_API = ("\(APINAME.init().DELETE_DESIGNATION_API)/\(String(describing: designation_ID!))")
        
        print(delete_API)
        WebService.requestDelUrl(strURL: delete_API, is_loader_required: true, params: [:], success: { (response) in
            
            print(response)
            if response["status_code"] as! NSNumber == 1 {
                
                DispatchQueue.main.async {
                    
                    self.designations_Array.remove(at: index_path)
                    self.home_collection_view.reloadData()
                    self.displayDeleteAlert(msg: response["message"] as! String)
                    
                }
            }
                
            else {
                  self.home_collection_view.reloadData()
                self.displayDeleteAlert(msg: response["message"] as! String)
            }
            
        }) { (error) in
            
            print(error)
        }
        
        
    }
    
    @objc func displayWarningAlert(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.home_collection_view)
        let indexPath = self.home_collection_view.indexPathForItem(at: point)
        let cell = self.home_collection_view.cellForItem(at: indexPath!) as! HomeCollectionViewCell

        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: "Are you sure?" , preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "delete", style: .default, handler: { (action) in
            
            self.delete_designation_API(index_path: (indexPath?.row)!)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action) in
            
            return
        }))
        
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayDeleteAlert(msg:String) {
        
       // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
  
    
    func displayAlert(msg:String) {
        
        let alert = UIAlertController()
   //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
       
        
        alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
            let modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ModeOfContactViewController") as! ModeOfContactViewController
            
            self.navigationController?.pushViewController(modeOfContact_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
       
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       return designations_Array.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
          //  let width = (self.view.frame.size.width - 60)/numberOfTimeCell
        let width =  (self.view.frame.size.width - 30)/numberOfTimeCell
        
        if user_data.User_Type != 1 {
            
        return CGSize(width: width, height: 130)
        }
        
            return CGSize(width: width, height: 160)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = home_collection_view.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        
      //  let borderColor = #colorLiteral(red: 0.9411764706, green: 0.1764705882, blue: 0.1882352941, alpha: 1).cgColor
      //  cell.main_view.layer.borderColor = borderColor
        cell.main_view.layer.masksToBounds = true
        
        cell.user_title_label.numberOfLines = 0
        cell.user_title_label.text = ((designations_Array[indexPath.row] )["name"] as! String)
        cell.main_view.layer.borderWidth = 1
        cell.main_view.clipsToBounds = true
        cell.user_image.layer.masksToBounds = true
        cell.user_image.layer.cornerRadius = 30
       // cell.user_image.layer.borderWidth = 1
//        cell.user_image.layer.borderColor = borderColor
//        cell.user_image.contentMode = .scaleToFill
//        cell.user_image.layer.isOpaque = false
//        cell.user_image.clipsToBounds = true
       //cell.buttons_view.layer.borderWidth = 1
        
        
        if user_data.User_Type != 1  {
            
            cell.edit_button.isHidden = true
            cell.delete_button.isHidden = true
        }
        
       
        cell.edit_button.addTarget(self, action: #selector(edit_button_action(_:)), for: .touchUpInside)
       cell.delete_button.addTarget(self, action: #selector(displayWarningAlert(_:)), for: .touchUpInside)
//        if (designations_Array[indexPath.row].object(forKey: "image") as! String).isEmpty == true {
//
//            //   cell.userImageView.setImage(string: user_data.user_first_name! + " " + user_data.user_last_name!)
//            print("Image hai ni")
//
//        }
//        else
//        {
//            if imagePath.isEmpty
//            {
//                imagePath = (designations_Array[indexPath.row].object(forKey: "image") as! String)
//            }
//            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
//            print(image_url as Any)
//
//            cell.user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
//
//            imagePath = ""
//        }
        
        
        
        return cell
    }
    
    @objc func edit_button_action(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.home_collection_view)
        let indexPath = self.home_collection_view.indexPathForItem(at: point)
     //   let cell = self.home_collection_view.cellForItem(at: indexPath!) as! HomeCollectionViewCell
        
        
        let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateDesignationViewController") as! UpdateDesignationViewController
        
        updateVC.designation_Array = designations_Array
        updateVC.designation_ID = designation_ID
        updateVC.designation_dict = (designations_Array[(indexPath?.row)!])
        self.navigationController?.pushViewController(updateVC, animated: true)
        
    }

   

}
