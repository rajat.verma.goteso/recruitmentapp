//
//  UpdateCandidateViewController.swift
//  Recruitment
//
//  Created by Apple on 17/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage


class UpdateCandidateViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var update_candidate_button: UIButton!
    @IBOutlet weak var user_image: UIImageView!
    let userDefaults = UserDefaults.standard
    
    var user_data:UserdataClass!
    var id : NSNumber!
    
    var index : Int!
    
    var candidate_list_array = [NSDictionary] ()
    private var datePicker : UIDatePicker?
   
    
    @IBOutlet weak var edit_img_button: UIButton!
    var data_dict = NSDictionary ()
    
    //    var record_data_placeholder = [NSDictionary(dictionaryLiteral: ("placeholder","Name of Candidate"),("value","")), "Gender" , "Designation" , "DOB" , "Mobile Number" , "Current Location" , "Hometown" , "Email" , "Current Salary" , "Expected Salary" , "Martial Status" , "Notice period" , "Resume" , "Whatsapp" , "Experience" , "Interview_Date_Time" , "Mode_Of_Contact" , "Verify_Status" , "Comments" , "Other_Details" , "Linkedin" , "Facebook" , "Twitter" , "Instagram" , "Github" , "Stackoverflow" , "Behance" , "Youtube" , "Created_By" ]
    
    var new_array = [NSDictionary(dictionaryLiteral: ("placeholder","Name of Candidate"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Gender"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","DOB"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Mobile Number"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Current Location"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Hometown"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Email"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Current Salary"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Expected Salary"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Martial Status"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Notice period"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Resume"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Whatsapp"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Experience"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Interview_Date_Time"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Mode Of Contact"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Comments"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Other_Details"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Linkedin"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Facebook"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Twitter"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Instagram"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Github"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Stackoverflow"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Behance"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Youtube"),("value","")),NSDictionary(dictionaryLiteral: ("placeholder","Verify Status"),("value",""))]
    
    var Year = ""
    var Month = ""
    var experience_data : Int!
     var experience_value = ""
    
    
    var name = ""
    var gender = ""
    var designation : NSNumber!
    var date_of_birth = ""
    var mobile = ""
    var current_address = ""
    var email = ""
    var current_salary = ""
    var expected_salary = ""
    var marital_status = ""
    var notice_period = ""
    var profile_pic = ""
    var resume = ""
    var whatsapp = ""
    var experience = ""
    var interview_date_time = ""
    var mode_of_contact = ""
    var varify_status = ""
    var comments = ""
    var other_details = ""
    var linkedin = ""
    var facebook = ""
    var twitter = ""
    var instagram = ""
    var github = ""
    var stackoverflow = ""
    var behance = ""
    var youtube = ""
    var created_by = ""
    var hometown = ""
    
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var components = ["123","456","789"]
    var gender_components = ["Select","Male" , "Female"]
    var current_salary_components = ["Select" ,"Fresher" , "< 1.0 L.P.A", "< 2.0 L.P.A" , "< 3.0 L.P.A" , "< 4.0 L.P.A" , "< 5.0 L.P.A" , "< 10.0 L.P.A" , "< 15.0 L.P.A" , "< 20.0 L.P.A" , "< 30.0 L.P.A" , "< 50.0 L.P.A"]
    var expected_salary_components = ["Select" , "< 1.0 L.P.A", "< 2.0 L.P.A" , "< 3.0 L.P.A" , "< 4.0 L.P.A" , "< 5.0 L.P.A" , "< 10.0 L.P.A" , "< 15.0 L.P.A" , "< 20.0 L.P.A" ,  "< 30.0 L.P.A" , "< 50.0 L.P.A"]
    var martial_components = ["Select","Single/Unmarried", "Married" , "Widowed" , "Divorced" , "Separated" , "In-a-relationship"]
    var notice_components = ["Select","Tomorrow" , "Day after Tomorrow" , "After 3 days" , "After 5 days" , "After 1 week"]
    var verify_components = ["Select","Verified", "Unverified"]
    var experience_components_one = ["Select","Fresher", "1 year" , "2 years" , "3 years" , "4 years" , "5 years" , "6 years" , "7 years" , "8 years" , "9 years" , "10 years"]
    var experience_components_two = [ "Select","1 month", "2 months" , "3 months" , "4 months" , "5 months" , "6 months", "7 months" , "8 months" , "9 months" , "10 months" , "11 months"]
    var modeOfContact_components = [String] ()
    var modeOfContact_Array = [NSDictionary] ()
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
    var image_url = ""
    var picker_type = ""
    
   
    @IBOutlet weak var Add_Record_Table_VIew: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(designation!)
        print(("\(String(describing: designation!))"))
        print(candidate_list_array)
        datePicker = UIDatePicker ()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(AddCandidateViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AddCandidateViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        
      
    print(imagePath)
        
    
            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
            print(image_url as Any)
            
            user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
       
        
        if userImage != nil
        {
            user_image.image = userImage
            user_image.contentMode = .scaleAspectFit
        }
        
        
        
        
        call_mode_of_contact_Api()
        
        if candidate_list_array.count > 0 {
            
            set_fields()
            
       
        }
        
    }
    
    
    func set_fields()  {
        
       if candidate_list_array.count > 0  {
        
        print(candidate_list_array)
      
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["name"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        name = (val as? String)!
                        
                    }
                    
                }
                
         
        
                
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["email"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                      email = (val as? String)!
                        
                    }
                    
              
            }
            
            
        
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["date_of_birth"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        date_of_birth = (val as? String)!
                      
                        
                    }
                    
                }
        
                
                
                for  item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["hometown"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                        hometown = (val as? String)!
                       
                    }
                }
        
            
        
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["current_address"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                       current_address = (val as? String)!
                        
                    }
                }
                
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["mobile"] {
                        
                        print(val)
                    mobile = (val as? String)!
                        
                    }
                }
        
       
                for  item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["whatsapp"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                      whatsapp = (val as? String)!
                        
                        
                    }
                    
                }
        
            
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["marital_status"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                      marital_status = (val as? String)!
                    }
                    
                }
        
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["gender"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                      gender = (val as? String)!
                        
                    }
                    
                }
        
                for item in candidate_list_array {

                    let data = item


                    if let val = data["current_salary"] {

                        print(val)

                        current_salary = ("< \(val).0 L.P.A")
//                        let selected_value = calculate_salary(value: current_salary)
//
//                        print(selected_value)

                    }

                }
        
                for item in candidate_list_array {

                    let data = item
                    if let val = data["expected_salary"] {

                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        expected_salary = ("< \(val).0 L.P.A")
//                       let selected_value = calculate_salary(value: expected_salary)
//
//                        print(selected_value)

                    }

                   }
        
        
                for item in candidate_list_array{
                    
                    let data = item
                    
                    if let val = data["interview_date_time"] {
                        
                        print(val)
                      interview_date_time = (val as? String)!
                    }
                    
                }
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["notice_period"] {
                        
                        print(val)
                        // print (self.candidate_list_array[indexPath.row].object(forKey: "name") as! String)
                        
                     notice_period = (val as? String)!
                    }
                    
                }
                
        
     
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["linkedin"] {
                        
                        print(val)
                     linkedin = (val as? String)!
                    }
                    
                }
        
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["instagram"] {
                        
                        print(val)
                        
                       instagram = (val as? String)!
                    }
                    
                }
        
            
        
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["facebook"] {
                        
                        print(val)
                 
                        
                  facebook = (val as? String)!
                        
                    }
                    
                }
     
        
                
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["twitter"] {
                        
                        print(val)
                       
                        twitter = (val as? String)!
                        
                    }
                    
                }
                
        
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["behance"] {
                        
                        print(val)
                       
                        behance = (val as? String)!
                        
                    }
                    
                }
        
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["github"] {
                        
                        print(val)
                       github = (val as? String)!
                    }
                    
                }
        
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["stackoverflow"] {
                        
                        print(val)
                        
                      stackoverflow = (val as? String)!
                    }
                    
                }
        
        
                for item in candidate_list_array {
                    
                    let data = item
                    
                    if let val = data["youtube"] {
                        
                        print(val)
                        
                      youtube = (val as? String)!
                        
                    }
                    
                }
        
            for item in candidate_list_array {
            
                 let data = item
            
                  if let val = data["experience"] {
                
                print(val)
                
                    let value = val as! Int
                    let year = (value/12)
                    
                    let month = (value % 12)
                    
                    if value == 0 {
                        
                        experience = "Fresher"
                    }
                    
                  else  if value < 12 && value != 0 {
                        
                        if month == 1 {
                            
                            experience = ("\(month) month")
                            
                        }
                            
                        else {
                            experience = ("\(month) months")
                            
                        }
                    }
                        
                    else {
                        
                        if year == 1 && month == 0 {
                            
                            experience = ("\(year) year")
                            
                        }
                            
                        else  if year == 1 && month == 1 {
                            
                            experience = ("\(year) year \(month) month")
                            
                        }
                            
                        else if year > 1 && month < 1 {
                            
                            experience = ("\(year) years")
                            
                        }
                            
                        else {
                            
                            experience = ("\(year) years \(month) months")
                            
                        }
                        
                    }
                    
                
                }
            
           }
        
        
        
        
        
        }
        
    }
    
    
    @objc func viewTapped(gestureRecognizer : UITapGestureRecognizer) {
        
        
        view.endEditing(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        
        self.user_image.layer.masksToBounds = true
        self.user_image.layer.cornerRadius = 65
        self.Add_Record_Table_VIew.register(UINib(nibName: "AddRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "AddRecordTableViewCell")
        edit_img_button.layer.masksToBounds = true
        edit_img_button.layer.cornerRadius = 13
        edit_img_button.addTarget(self, action: #selector(edit_image_button_action(_:)), for: .touchUpInside)
        
        if userImage != nil
        {
            user_image.image = userImage
            user_image.contentMode = .scaleToFill
        }
        
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title =  "Update Candidate"
        addBackButton()
        
        update_candidate_button.addTarget(self, action: #selector(update_candidate_API(_:)), for: .touchUpInside)
        
        
    }
    
    
    internal var btnDropDown: UIButton {
        let size: CGFloat = 20.0
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "drop_down"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        //   button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    
    internal var btnFilePicker: UIButton {
        let size: CGFloat = 20.0
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "filePicker"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        button.addTarget(self, action: #selector(FILE_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        
        if picker_type == "Experience" {
            
            return 2
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if  picker_type == "Gender" {
            return gender_components.count
        }
        if picker_type == "Current Salary"  {
            
            
            return current_salary_components.count
        }
        
        if picker_type == "Expected Salary"  {
            
            
            return expected_salary_components.count
        }
        
        if picker_type == "Martial Status" {
            
            return martial_components.count
        }
        
        if picker_type == "Experience" {
            if component == 0 {
                
                return experience_components_one.count
                
            }
            else {
                
                return experience_components_two.count
                
            }
        }
        if picker_type == "Mode Of Contact" {
            
            return modeOfContact_components.count
        }
        
        if picker_type == "Verify Status" {
            
            return verify_components.count
        }
        
        return notice_components.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if  picker_type == "Gender" {
            return gender_components[row]
        }
        if picker_type == "Current Salary" {
            
            
            return current_salary_components[row]
        }
        if picker_type == "Expected Salary" {
            
            
            return expected_salary_components[row]
        }
        
        if picker_type == "Martial Status" {
            
            return martial_components[row]
        }
        
        if picker_type == "Experience" {
            
            if (component == 0) {
                
                return experience_components_one[row]
                
            }
            else {
                return experience_components_two[row]
            }
           
        }
        
        if picker_type == "Notice period" {
            
            return notice_components[row]
        }
        if picker_type == "Mode Of Contact" {
            
            return modeOfContact_components[row]
        }
        if picker_type == "Verify Status" {
            
            return verify_components[row]
        }
        return notice_components[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if picker_type == "Gender" {
            if gender_components[row] == "Select" {
                
                gender = ""
                
            }
            else {
                
                gender = gender_components[row]
                
            }
        }
        
        if picker_type == "Current Salary" {
           
 
             current_salary = current_salary_components[row]
           
            print(current_salary)
           
        }
        if picker_type == "Expected Salary" {
            
             expected_salary = expected_salary_components[row]
           print(expected_salary)
          
        }
        if picker_type == "Experience" {
            
            if component == 0 {
                
                Year = experience_components_one[row]
                
                if Year == "Select" {
                    
                    Year = ""
                    
                }
                
                if Year == "Fresher" {
                    
                    Month = ""
                }
                
            }
            
            if component == 1 {
                
                print(Year)
                if Year == "Fresher"{
                    
                    Month = ""
                    displayAlert(msg: "You Selected Fresher")
                }
                    
                else {
                    
                    Month = experience_components_two[row]
                    
                    if Month == "Select" {
                        
                        Month = ""
                    }
                    
                }
                
            }
            
            if Month == "" {
                
                experience = Year
                
                let year1 = calculate_experience(value: Year)
                print(year1)
                
                experience_data = (year1*12)
                print(experience_data!)
                
            }
            else if Year == "" {
                
                experience = Month
                let month1 = calculate_experience(value: Month)
                print(month1)
                experience_data = (month1)
                print(experience_data!)
                
            }
            else {
                experience = ("\(Year) \(Month)")
                
                let year1 = calculate_experience(value: Year)
                let month1 = calculate_experience(value: Month)
                
                print(year1)
                print(month1)
                
                experience_data = (year1*12) + (month1)
                print(experience_data!)
                
            }
            print(experience)
            
        }
        if picker_type == "Martial Status" {
            
            if martial_components[row] == "Select" {
                
                marital_status = ""
            }
            else {
            
            marital_status = martial_components[row]
            }
        }
        if picker_type == "Notice period" {
            
            if notice_components[row] == "Select" {
                
                notice_period = ""
                
            }
            else {
                
                notice_period = notice_components[row]
                
            }
        }
        if picker_type == "Mode Of Contact" {
            
            if modeOfContact_components[row] == "Select" {
                
                mode_of_contact = "Select"
            }
            
            else {
                
                 mode_of_contact = modeOfContact_components[row]
            }
            
        }
        if picker_type == "Verify Status" {
            if verify_components[row] == "Select" {
                
                varify_status = ""
                
            }
            else {
                
                varify_status = verify_components[row]
                
            }
        }
        
        
        self.Add_Record_Table_VIew.reloadData()
        self.view.endEditing(true)
    }
    
    func calculate_experience(value : String) -> Int{
        
        
        if value == "Fresher"{
            
            return 0
        }
        
        if value == "1 year" || value == "1 month" {
            
            return 1
        }
        
        if value == "2 years" || value == "2 months" {
            
            return 2
        }
        if value == "3 years" || value == "3 months" {
            
            return 3
        }
        if value == "4 years" || value == "4 months" {
            
            return 4
        }
        if value == "5 years" || value == "5 months" {
            
            return 5
        }
        
        if value == "6 years" || value == "6 months" {
            
            return 6
        }
        if value == "7 years" || value == "7 months" {
            
            return 7
        }
        if value == "8 years" || value == "8 months" {
            
            return 8
        }
        if value == "9 years" || value == "9 months" {
            
            return 9
        }
        if value == "10 years" || value == "10 months" {
            
            return 10
        }
        
        if value == "11 months" {
            
            return 11
        }
            
        else{
            
            return 0
        }
        
    }
    
    func calculate_salary(value : String) -> Int {
        
        if value == "Fresher"{
            
            return 0
        }
        
        if value == "< 1.0 L.P.A" {
            
            return 1
        }
        
        if value == "< 2.0 L.P.A" {
            
            return 2
        }
        if value == "< 3.0 L.P.A"  {
            
            return 3
        }
        if value == "< 4.0 L.P.A"  {
            
            return 4
        }
        if value == "< 5.0 L.P.A"  {
            
            return 5
        }
        
        if value == "< 10.0 L.P.A"  {
            
            return 10
        }
        if value == "< 15.0 L.P.A"  {
            
            return 15
        }
        if value == "< 20.0 L.P.A"  {
            
            return 20
        }
        if value == "< 25.0 L.P.A"  {
            
            return 25
        }
        if value == "< 30.0 L.P.A"  {
            
            return 30
        }
        
        else{
            
            return 0
        }
        
        
        
    }
    
    
    
    @objc
    func update_candidate_API(_ sender : UIButton) {
        
        
        if name == "" {
            
            displayAlert(msg: "Enter name")
            
        }
        
        
        if mobile == "" {
            
            displayAlert(msg: "Enter Mobile")
        }

        else {
            
           
            
            print(experience_data)
            if experience_data == nil {
                
                experience_value = ""
            }
                
            else {
                
                experience_value = send_experience(value: experience_data)
                
            }

                var VERIFY_STATUS : NSNumber!
                
                if varify_status == "Verified" {
                    VERIFY_STATUS = 1
                }
                if varify_status == "Unverified" {
                    
                    VERIFY_STATUS = 0
                }
            
            
           let salary_current = calculate_salary(value: current_salary)
          let salary_expected = calculate_salary(value: expected_salary)
            
            print(salary_current)
            print(salary_expected)
 
                let update_Candidate_API = APINAME.init().UPDATE_CANDIDATE_API + ("/\(id!)")
                let params = ["name" : name ,"email" : email , "mobile" : mobile ,"profile_pic" : imagePath, "gender" : gender , "resume" : "resume/1570881243.docx" , "marital_status" : marital_status , "date_of_birth" : date_of_birth , "whatsapp" : whatsapp , "hometown" : hometown, "current_address" : current_address , "designation"  : ("\(designation!)") , "experience" : experience_value , "current_salary" : salary_current, "expected_salary" : salary_expected, "notice_period" : notice_period , "interview_date_time" : interview_date_time , "mode_of_contact" : mode_of_contact, "comments" : comments , "other_details" : other_details , "linkedin" : linkedin , "facebook" : facebook , "twitter" : twitter , "instagram" : instagram , "github" : github , "stackoverflow" : stackoverflow, "behance" : behance , "youtube" : youtube , "verify_status" : VERIFY_STATUS]  as [String : Any]
                
                print(params)
                
                print(imagePath)
                
                print(update_Candidate_API)
                WebService.requestPUTUrlWithJSONDictionaryParameters(strURL: update_Candidate_API, is_loader_required: true, params: (params as NSDictionary) as! [String : Any], success: { (response) in
                    
                    print(response)
                    if response["status_code"] as! NSNumber == 1 {
                        
                    self.displayAlertAction(msg: response["message"] as! String)
                    }
                    else {
                        
                        self.displayAlert(msg: response["message"] as! String)
                        
                    }
                    
                }) { (error) in
                    
                    print(error)
                    
                }
//            }
//
//            else {
//
//                displayAlert(msg: "Please select image")
//
//            }
            
            
        }
    }
    
    
    func send_experience(value : Int) -> String {
        
        if value != nil {
            
            return ("\(value)")
            
        }
        
        return ""
        
    }
    func call_mode_of_contact_Api() {
        
        let mode_of_contact_API = APINAME.init().MODE_OF_CONTACT_API
        
        WebService.requestGetUrlWithoutParameters(strURL: mode_of_contact_API, is_loader_required: false, success: { (response) in
            
            print(response)
            
            self.modeOfContact_Array = response["data"] as! [NSDictionary]
            for item in self.modeOfContact_Array {
                
                self.modeOfContact_components.append("Select")
                self.modeOfContact_components.append((item )["title"] as! String)
                
            }
            print(self.modeOfContact_components)
            
            DispatchQueue.main.async {
                self.Add_Record_Table_VIew.reloadData()
            }
            
        }) { (error) in
            
            print(error)
            
        }
        
        
    }
    
    
    @objc func YOUR_BUTTON__TAP_ACTION(_ sender: UIButton) {
        
        let index  = sender.tag
        
        print((new_array[index]))
        
        if (new_array[index])["placeholder"] as! String == "Gender" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Current Salary" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Expected Salary" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Martial Status" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Notice period" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Experience" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        if (new_array[index])["placeholder"] as! String == "Mode Of Contact" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        if (new_array[index])["placeholder"] as! String == "Verify Status" {
            
            picker_type = (new_array[index])["placeholder"] as! String
        }
        
        picker = UIPickerView.init()
        picker.delegate = self
        // picker.backgroundColor = UIColor.lightGray
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.setValue(UIColor.white, forKey: "backgroundColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
        
    }
    
    @objc func FILE_BUTTON__TAP_ACTION(_ sender: UIButton) {
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
        
    }
    
    
    @objc func onDoneButtonTapped() {
        
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    @objc func dateChanged(datePicker : UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        date_of_birth = dateFormatter.string(from: datePicker.date)
        
        print(date_of_birth)
        
        self.Add_Record_Table_VIew.reloadData()
        view.endEditing(true)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return new_array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Add_Record_Table_VIew.dequeueReusableCell(withIdentifier: "AddRecordTableViewCell") as! AddRecordTableViewCell
        
        cell.tag = indexPath.row
        cell.record_text_field.tag = indexPath.row
        cell.selectionStyle = .none
        cell.record_text_field.setBottomBorder(withColor: UIColor.lightGray)
        cell.record_text_field.font = UIFont(name: "Open Sans", size: 12)
        cell.record_text_field.placeholder = ((new_array[indexPath.row] )["placeholder"] as! String)
        cell.record_text_field.rightView = self.btnDropDown
        cell.record_text_field.rightViewMode = .always
        
        cell.record_text_field.delegate = self
        if cell.record_text_field.placeholder == "Name of Candidate" {
            
            cell.label.text = "Name :"
            cell.img_icon.image = UIImage(named: "designation")
             cell.record_text_field.rightView?.isHidden = true
            cell.record_text_field.keyboardType = .default
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            print(cell.record_text_field.text!)
            print(name)
            
            if name != "" {
                
                cell.record_text_field.text! = name
                print(name)
                
            }
            
               name = cell.record_text_field.text! 
          
        }
        
        if cell.record_text_field.placeholder == "Gender" {
            
            //  cell.record_text_field.inputView = picker
             cell.label.text = "Gender :"
            cell.img_icon.image = UIImage(named: "gender")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
              cell.button.isHidden = false
            
            if gender != "" {
                
                cell.record_text_field.text! = gender
                
            }
            
            else {
                
                cell.record_text_field.text! = "Select"
                
            }
            
          //  gender = cell.record_text_field.text!
            
        }
        
        
        if cell.record_text_field.placeholder == "DOB" {
            
            //   cell.record_text_field.inputView = picker
             cell.label.text = "Date Of Birth :"
            cell.img_icon.image = UIImage(named: "dob")
            cell.button.isHidden = true
            cell.record_text_field.isUserInteractionEnabled = true
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.record_text_field.inputView = datePicker
            
            if date_of_birth != "" {
                
                
                cell.record_text_field.text! = date_of_birth
                
            }
            else {
                
                cell.record_text_field.text! = "Select"
                
            }
            
//            date_of_birth = cell.record_text_field.text!
//            cell.record_text_field.text! = date_of_birth
            
        }
        
        
        if cell.record_text_field.placeholder == "Mobile Number" {
            
            // cell.record_text_field.inputView = picker
             cell.label.text = "Mobile Number :"
            cell.img_icon.image = UIImage(named: "mobile_number")
            cell.record_text_field.keyboardType = .phonePad
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if mobile != "" {
                
                cell.record_text_field.text = mobile
                
            }
            
            mobile = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Current Location" {
             cell.label.text = "Current Location :"
            
            cell.img_icon.image = UIImage(named: "location")
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.record_text_field.keyboardType = .default
             cell.button.isHidden = true
            
            
            if current_address != "" {
                
                cell.record_text_field.text = current_address
                
            }
            
            current_address = cell.record_text_field.text!
            
        }
        
        
        if cell.record_text_field.placeholder == "Email" {
            
            print(email)
             cell.label.text = "Email :"
            cell.img_icon.image = UIImage(named: "email")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if email != "" {
                
                print(email)
                cell.record_text_field.text = email
                
            }
            email = cell.record_text_field.text!
            
        }
        
        
        if cell.record_text_field.placeholder == "Hometown" {
            
             cell.label.text = "Hometown :"
            cell.img_icon.image = UIImage(named: "hometown")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if hometown != "" {
                
                cell.record_text_field.text = hometown
                
            }
            hometown = cell.record_text_field.text!
            
        }
        if cell.record_text_field.placeholder == "Current Salary" {
            
            print(current_salary)
             cell.label.text = "Current Salary :"
            cell.img_icon.image = UIImage(named: "current_salary")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            
            cell.button.isHidden = false
            
            
            if current_salary != "" {
                
                if current_salary == "< 0.0 L.P.A" {
                    
                    print(current_salary)
                    cell.record_text_field.text = "Fresher"
                    
                }
                  
                else {
                
                print(current_salary)
                cell.record_text_field.text = current_salary
            }
            
            }
            
            else {
                    print(current_salary)
                    cell.record_text_field.text = "Select"
                
            }
        //    current_salary = Int(cell.record_text_field.text!)
            
        }
        
        if cell.record_text_field.placeholder == "Expected Salary" {
            
            print(expected_salary)
             cell.label.text = "Expected Salary :"
            cell.img_icon.image = UIImage(named: "expected_salary")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            
            cell.button.isHidden = false
            
            
            if expected_salary != "" {
                
                if expected_salary == "< 0.0 L.P.A" {
                    
                    print(expected_salary)
                    cell.record_text_field.text = "Select"
                    
                }
                
                else {
                    
                print(expected_salary)
                cell.record_text_field.text = expected_salary
                    
                
                }
            }
            
            else {
                
                    print(expected_salary)
                    cell.record_text_field.text = "Select"
               
            }
            
        //    expected_salary = Int(cell.record_text_field.text!)
            
            
        }
        
        if cell.record_text_field.placeholder == "Martial Status" {
            
             cell.label.text = "Martial Status :"
            cell.img_icon.image = UIImage(named: "martial_status")
            cell.record_text_field.rightView?.isHidden = false
            
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
            
            if marital_status != "" {
                
                cell.record_text_field.text = marital_status
            }
            
            else {
                
                cell.record_text_field.text! = "Select"
                
            }
           // marital_status = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Notice period" {
            
             cell.label.text = "Notice Period :"
            cell.img_icon.image = UIImage(named: "notice_period")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled = false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            
            cell.button.isHidden = false
            if notice_period != "" {
                
                cell.record_text_field.text = notice_period
                
            }
            
            else {
                
                cell.record_text_field.text! = "Select"
                
            }
//            cell.button.isHidden = false
//            cell.button.tag = indexPath.row
//
           // notice_period = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Resume" {
            
            //  cell.record_text_field.inputView = picker
             cell.label.text = "Resume :"
            cell.img_icon.image = UIImage(named: "resume")
            cell.record_text_field.rightView = self.btnFilePicker
            cell.record_text_field.rightViewMode = .always
            cell.record_text_field.rightView?.isHidden = false
            cell.button.isHidden = false
            
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            
            if resume != "" {
                cell.record_text_field.text = resume
                
            }
            resume = cell.record_text_field.text!
           
        }
        
        if cell.record_text_field.placeholder == "Whatsapp" {
            
             cell.label.text = "Whatsapp :"
            cell.img_icon.image = UIImage(named: "whatsapp")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.keyboardType = .phonePad
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if whatsapp != "" {
                
                cell.record_text_field.text = whatsapp
            }
            whatsapp = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Experience" {
            
            print(experience)
            //   cell.record_text_field.inputView = picker
             cell.label.text = "Experience :"
            cell.img_icon.image = UIImage(named: "experience")
            //     cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
            
            if experience != "" {
                cell.record_text_field.text = experience
                
            }
            
            else {
                
                cell.record_text_field.text = "Select"
            }
           // experience = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Interview_Date_Time" {
            
            // cell.record_text_field.inputView = picker
             cell.label.text = "Interview Date&Time :"
            cell.img_icon.image = UIImage(named: "time_card")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if interview_date_time != "" {
                
                cell.record_text_field.text = interview_date_time
            }
            
            else {
                
                cell.record_text_field.text = "Select"
            }
          //  interview_date_time = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Mode Of Contact" {
            
             cell.label.text = "Mode Of Contact :"
            cell.img_icon.image = UIImage(named: "information")
            cell.record_text_field.rightView?.isHidden = false
            cell.record_text_field.isUserInteractionEnabled =  false
            cell.record_text_field.rightView?.isUserInteractionEnabled = false
            cell.button.isHidden = false
            
            if mode_of_contact != "" {
                cell.record_text_field.text = mode_of_contact
                
            }
            
            else {
                
                cell.record_text_field.text = "Select"
            }
          //  mode_of_contact = cell.record_text_field.text!
            
        }
        
                 if cell.record_text_field.placeholder == "Verify Status" {
        
                     cell.label.text = "Verify Status :"
                    cell.img_icon.image = UIImage(named: "verify_status")
                    cell.record_text_field.rightView?.isHidden = false
                    cell.record_text_field.isUserInteractionEnabled =  false
                    cell.record_text_field.rightView?.isUserInteractionEnabled = false
                    cell.button.isHidden = false
        
                    if varify_status != "" {
                         cell.record_text_field.text = varify_status
        
                    }
        
                    else {
                        
                        cell.record_text_field.text = "Select"
                    }
                  //  varify_status = cell.record_text_field.text!
        
                }
        
        if cell.record_text_field.placeholder == "Comments" {
            
             cell.label.text = "Comments :"
            cell.img_icon.image = UIImage(named: "comments")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if comments != "" {
                cell.record_text_field.text = comments
                
            }
            comments = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Other_Details" {
            
             cell.label.text = "Other Details :"
            cell.img_icon.image = UIImage(named: "moreDetails")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if other_details != "" {
                
                cell.record_text_field.text = other_details
                
            }
            other_details = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Linkedin" {
            
             cell.label.text = "Linkedin :"
            cell.img_icon.image = UIImage(named: "linkedin")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if linkedin != "" {
                
                cell.record_text_field.text = linkedin
                
            }
            linkedin = cell.record_text_field.text!
            
        }
        
        if cell.record_text_field.placeholder == "Facebook" {
            
             cell.label.text = "Facebook :"
            cell.img_icon.image = UIImage(named: "face_book")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if facebook != "" {
                
                cell.record_text_field.text = facebook
                
            }
            facebook = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Twitter" {
            
             cell.label.text = "Twitter :"
            cell.img_icon.image = UIImage(named: "tweet")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if twitter != "" {
                cell.record_text_field.text = twitter
                
            }
            twitter = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Instagram" {
            
             cell.label.text = "Instagram :"
            cell.img_icon.image = UIImage(named: "instagram")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if instagram != "" {
                cell.record_text_field.text = instagram
                
            }
            instagram = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Github" {
            
             cell.label.text = "Github :"
            cell.img_icon.image = UIImage(named: "github")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if github != "" {
                
                cell.record_text_field.text = github
            }
            github = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Stackoverflow" {
            
             cell.label.text = "Stackoverflow :"
            cell.img_icon.image = UIImage(named: "stack_overflow")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if stackoverflow != "" {
                
                cell.record_text_field.text = stackoverflow
                
            }
            stackoverflow = cell.record_text_field.text!
            
        }
        if cell.record_text_field.placeholder == "Behance" {
            
             cell.label.text = "Behance :"
            cell.img_icon.image = UIImage(named: "behance")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
             cell.button.isHidden = true
            
            if behance != "" {
                
                cell.record_text_field.text = behance
                
            }
            behance = cell.record_text_field.text!
            
            
        }
        
        if cell.record_text_field.placeholder == "Youtube" {
            
             cell.label.text = "Youtube :"
            cell.img_icon.image = UIImage(named: "youtube")
             cell.record_text_field.keyboardType = .default
            cell.record_text_field.rightView?.isHidden = true
             cell.record_text_field.isUserInteractionEnabled = true
            cell.button.isHidden = true
            
            
            if youtube != "" {
                
                cell.record_text_field.text = youtube
                
            }
            youtube = cell.record_text_field.text!
            
            
        }
        
        
        cell.button.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlertAction(msg:String) {
        
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            self.navigationController?.popViewController(animated: true)
            
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
}

extension UpdateCandidateViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        print(newString)
        
        let index = textField.tag
        
        print(index)
        
        
        if ((new_array[index] )["placeholder"] as! String) == "Name of Candidate" {
            
            name = newString
            print(name)
            
            let userInput = newString
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            if userInput.rangeOfCharacter(from: set.inverted) != nil {
                print("ERROR: There are numbers included!")
                displayAlert(msg: "Name requires alphabets only.")
                
            }
            
            else {
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(newString, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
            }
          
            
        }
        
       
        
        
        if ((new_array[index] )["placeholder"] as! String) == "Mobile Number" {
            
            mobile = newString
            // cell.record_text_field.inputView = picker
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(newString, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
        
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Current Location" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                current_address = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Email" {
            
            email = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        
        if ((new_array[index] )["placeholder"] as! String) == "Hometown" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                hometown = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
          
        }
    
        if ((new_array[index] )["placeholder"] as! String) == "Resume" {
            
            resume = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
         
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Whatsapp" {
            
            whatsapp = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
         
        }
       
        if ((new_array[index] )["placeholder"] as! String) == "Interview_Date_Time" {
            
            interview_date_time = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
      
     
        if ((new_array[index] )["placeholder"] as! String) == "Comments" {
            
            comments = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Other_Details" {
            
            
            other_details = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Linkedin" {
            
            linkedin = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
          
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Facebook" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                facebook = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
          
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Twitter" {
            
            twitter = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
         
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Instagram" {
            
            instagram = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Github" {
            
            github = newString
            
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Stackoverflow" {
            if ((new_array[index] )["value"] as! String) == "" {
                
                stackoverflow = newString
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
         
        }
        if ((new_array[index] )["placeholder"] as! String) == "Behance" {
            
            behance = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
            
        }
        
        if ((new_array[index] )["placeholder"] as! String) == "Youtube" {
            
            youtube = newString
            if ((new_array[index] )["value"] as! String) == "" {
                
                let data_dict =  (new_array[index] ).mutableCopy() as! NSMutableDictionary
                
                data_dict.setValue(textField.text!, forKey: "value")
                
                new_array[index] = data_dict
                
            }
                
            else {
                
                print(((new_array[index] )["value"] as! String))
                
            }
           
        }
        
        
        return true
        
        
    }
    
    
    
}

extension UpdateCandidateViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
        self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            if JSON["status_code"] as! NSNumber == 0 {
                                
                                self.displayAlert(msg: JSON["message"] as! String)
                                
                            }
                                
                            else {
                                
                                self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                                print(self.imagePath)
                                DispatchQueue.main.async {
                                    let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                    if image_url != nil
                                    {
                                        if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                        {
                                            self.userImage = UIImage(data: data1 as Data)
                                        }
                                        
                                    }
                                    
                                    SVProgressHUD.dismiss()
                                    self.reloadInputViews()
                                    //     self.tableView.reloadData()
                                }
                                
                                print(JSON)
                            }
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
    
    
    
}

extension UpdateCandidateViewController: UIDocumentPickerDelegate{
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        
        print(url.pathExtension)
        
    }
}
