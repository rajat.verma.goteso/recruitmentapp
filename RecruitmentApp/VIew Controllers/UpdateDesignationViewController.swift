//
//  UpdateDesignationViewController.swift
//  Recruitment
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class UpdateDesignationViewController: UIViewController {

    @IBOutlet weak var user_image: UIImageView!
    
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var edit_userImage_button: UIButton!
    
    @IBOutlet weak var enter_designation_text_field: UITextField!
    
    @IBOutlet weak var update_button: UIButton!
    var designation_ID : NSNumber!
    var designation_Array = [NSDictionary] ()
    var designation_dict = NSDictionary()
    var designation_title = ""
    
    var name = ""
    var image_url = ""
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(designation_dict)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Update Designation"
        addBackButton()
        
      //  edit_userImage_button.addTarget(self, action: #selector(edit_image_button_action(_:)), for: .touchUpInside)
        update_button.addTarget(self, action: #selector(updateDesignation_API(_:)), for: .touchUpInside)
        
        
        if designation_title == "" {
            
            designation_title = designation_dict.object(forKey: "name") as!String
        }
        
        enter_designation_text_field.text = designation_title
        enter_designation_text_field.delegate = self
        
        
        
//        if (designation_dict.object(forKey: "image") as! String).isEmpty == true {
//
//            //   cell.userImageView.setImage(string: user_data.user_first_name! + " " + user_data.user_last_name!)
//            print("Image hai ni")
//
//        }
//        else
//        {
//            if imagePath.isEmpty
//            {
//                imagePath = (designation_dict.object(forKey: "image") as! String)
//            }
//            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
//            print(image_url as Any)
//
//           user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
//        }
//
//        if userImage != nil
//        {
//           user_image.image = userImage
//            user_image.contentMode = .scaleAspectFit
//        }
//
//
        
    }
    
    override func viewDidLayoutSubviews() {
        
        update_button.layer.masksToBounds = true
        update_button.layer.cornerRadius = 3
        enter_designation_text_field.setBottomBorder(withColor: UIColor.lightGray)
//        user_image.layer.masksToBounds = true
//        user_image.layer.cornerRadius = 50
//        user_image.clipsToBounds = true
//        user_image.contentMode = .scaleToFill
        main_view.layer.borderWidth = 1
//        edit_userImage_button.layer.masksToBounds = true
//        edit_userImage_button.layer.cornerRadius = 15
        
        
    }
    
    @objc
    func updateDesignation_API(_ sender : UIButton) {

        
        if enter_designation_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Designation")
        }
        else {
            
            print(enter_designation_text_field.text as Any)
//            if imagePath != "" {
            
                let update_API = ("\(APINAME.init().UPDATE_DESIGNATION_API)/\(String(describing: designation_dict.object(forKey: "id")!))")
                let params = ["name" : enter_designation_text_field.text!] as [String : Any]
                
                print(update_API)
                print(imagePath)
              
                WebService.requestPUTUrlWithJSONDictionaryParameters(strURL: update_API, is_loader_required: true, params: params, success: { (response) in
                    if response["status_code"] as! NSNumber == 1 {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else {
                        
                        self.displayAlert(msg: response["message"] as! String)
                        
                    }
                }) { (error) in
                    
                    print(error)
                }
                
                
//            }
//
//            else {
//
//                displayAlert(msg: "Please select image")
//
//            }
            
        }
        
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }


}


extension UpdateDesignationViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
        self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
        // apiMultipart(image_data: user_image)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                          
                            if JSON["status_code"] as! NSNumber == 0 {
                                
                                self.displayAlert(msg: JSON["message"] as! String)
                                
                            }
                            else {
                                
                                self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                                print(self.imagePath)
                                DispatchQueue.main.async {
                                    let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                    if image_url != nil
                                    {
                                        if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                        {
                                            self.userImage = UIImage(data: data1 as Data)
                                        }
                                        
                                    }
                                    
                                    SVProgressHUD.dismiss()
                                    self.reloadInputViews()
                                    //     self.tableView.reloadData()
                                }
                                
                                print(JSON)
                            }
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
}

extension UpdateDesignationViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        textField.returnKeyType = .done
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
        
    }
    
    
}
