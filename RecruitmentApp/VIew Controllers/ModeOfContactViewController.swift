//
//  ModeOfContactViewController.swift
//  Recruitment
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ModeOfContactViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {

    var numberOfTimeCell : CGFloat { if UIDevice.current.userInterfaceIdiom == .pad
    {   return 4    }
    else{        return 2 }
    }
    
    @IBOutlet weak var modeOfContact_collection_view: UICollectionView!
    
    @IBOutlet weak var add_button: UIButton!
    
    var modeOfContact_Array = [NSDictionary] ()
    var modeOfContact_dict : NSDictionary!
    var imagePath = ""
    var modeOfContact_ID : NSNumber!
    var window : UIWindow?
    var user_data:UserdataClass!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        add_button.layer.masksToBounds = true
        add_button.layer.cornerRadius = 20
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Mode Of Contact"
        modeOfContact_collection_view.register(UINib(nibName: "ModeOfContactCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ModeOfContactCollectionViewCell")
        
       
//        let button = UIButton(type: .custom)
//        button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
//        button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
//        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
//        button.clipsToBounds = true
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.rightBarButtonItem = barButton
        
        if  user_data.User_Type != 1 {
            
            add_button.isHidden = true
        }
        
        call_modeOfContact_API()
        self.add_button.addTarget(self, action: #selector(add_button_action(_:)), for: .touchUpInside)
        
    }
    
    @objc func add_button_action(_ sender: UIButton) {
        
        
        let add_modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewModeOfContactViewController") as! AddNewModeOfContactViewController


        self.navigationController?.pushViewController(add_modeOfContact_VC, animated: true)
        
    }
    
    func call_modeOfContact_API() {
        
        let modeOfContact_API = APINAME.init().MODE_OF_CONTACT_API
        
        print(modeOfContact_API)
        WebService.requestGetUrlWithoutParameters(strURL: modeOfContact_API, is_loader_required: true, success: { (response) in
            
            print(response)
            
            self.modeOfContact_Array = response["data"] as! [NSDictionary]
            
            
            
            DispatchQueue.main.async {
                
                self.modeOfContact_collection_view.reloadData()
            }
            
        }) { (error) in
            
            
            print(error)
        }
        
        
        
    }
    
    
    @objc
    
    func rightButtonAction(sender: Any) {
        
        displayAlert(msg: "Choose")
        
    }
    
    func delete_modeOfContact_API(index_path : Int) {
        
        modeOfContact_ID = (modeOfContact_Array[index_path].object(forKey: "id") as! NSNumber)
        
        let delete_API = ("\(APINAME.init().DELETE_MODE_OF_CONTACT_API)/\(String(describing: modeOfContact_ID!))")
        
        print(delete_API)
        WebService.requestDelUrl(strURL: delete_API, is_loader_required: true, params: [:], success: { (response) in
            
            print(response)
            DispatchQueue.main.async {
                
                
                self.modeOfContact_Array.remove(at: index_path)
                self.modeOfContact_collection_view.reloadData()
                self.displayDeleteAlert(msg: "Successfully deleted")
                
            }
            
        }) { (error) in
            
            print(error)
        }
        
        
    }
    
    func displayDeleteAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    

    @objc func displayWarningAlert(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.modeOfContact_collection_view)
        let indexPath = self.modeOfContact_collection_view.indexPathForItem(at: point)
        let cell = self.modeOfContact_collection_view.cellForItem(at: indexPath!) as! ModeOfContactCollectionViewCell
        
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: "Are you sure?" , preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "delete", style: .default, handler: { (action) in
            
            self.delete_modeOfContact_API(index_path: (indexPath?.row)!)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action) in
            
            return
        }))
        
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlert(msg:String) {
        
        let alert = UIAlertController()
        //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
            let modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ModeOfContactViewController") as! ModeOfContactViewController
            
            self.navigationController?.pushViewController(modeOfContact_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return modeOfContact_Array.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //  let width = (self.view.frame.size.width - 60)/numberOfTimeCell
        let width =  (self.view.frame.size.width - 30)/numberOfTimeCell
        
        return CGSize(width: width, height: 150)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = modeOfContact_collection_view.dequeueReusableCell(withReuseIdentifier: "ModeOfContactCollectionViewCell", for: indexPath) as! ModeOfContactCollectionViewCell
        
        
        //  let borderColor = #colorLiteral(red: 0.9411764706, green: 0.1764705882, blue: 0.1882352941, alpha: 1).cgColor
        //  cell.main_view.layer.borderColor = borderColor
        cell.main_view.layer.masksToBounds = true
        
        cell.title.numberOfLines = 0
        cell.title.text = ((modeOfContact_Array[indexPath.row] )["title"] as! String)
        cell.main_view.layer.borderWidth = 1
        cell.main_view.clipsToBounds = true
//        cell.user_image.layer.masksToBounds = true
//        cell.user_image.layer.cornerRadius = 30
        // cell.user_image.layer.borderWidth = 1
        //        cell.user_image.layer.borderColor = borderColor
//        cell.user_image.contentMode = .scaleToFill
//        cell.user_image.layer.isOpaque = false
//        cell.user_image.clipsToBounds = true
        //cell.buttons_view.layer.borderWidth = 1
        
        if user_data.User_Type != 1 {
            
            cell.edit_button.isHidden = true
            cell.delete_button.isHidden = true
            
        }
        
        cell.edit_button.addTarget(self, action: #selector(edit_button_action(_:)), for: .touchUpInside)
        cell.delete_button.addTarget(self, action: #selector(displayWarningAlert(_:)), for: .touchUpInside)
     
        
        return cell
    }
    
    @objc func edit_button_action(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.modeOfContact_collection_view)
        let indexPath = self.modeOfContact_collection_view.indexPathForItem(at: point)
        //   let cell = self.home_collection_view.cellForItem(at: indexPath!) as! HomeCollectionViewCell
        
        
        let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateModeOfContactViewController") as! UpdateModeOfContactViewController
        
        updateVC.modeOfContact_Array = modeOfContact_Array
        updateVC.modeOfContact_dict = (modeOfContact_Array[(indexPath?.row)!])
        self.navigationController?.pushViewController(updateVC, animated: true)
        
    }

}
