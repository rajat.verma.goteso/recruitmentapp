//
//  ProfileViewController.swift
//  Recruitment
//
//  Created by Apple on 10/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ProfileViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
   
    @IBOutlet weak var add_button: UIButton!
    var numberOfTimeCell : CGFloat { if UIDevice.current.userInterfaceIdiom == .pad
    {   return 2    }
    else{        return 1 }
    }
    
    var profile_dict : NSDictionary!
    var imagePath = ""
    var profile_ID : NSNumber!
    var profile_Array = [NSDictionary] ()
    var user_type = NSNumber()
    var user_data:UserdataClass!
    
    var window : UIWindow?
    
    @IBOutlet weak var add_new_profile_button: UIButton!
    
    @IBOutlet weak var profile_collection_view: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Profile"
          profile_collection_view.register(UINib(nibName: "ProfileCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProfileCollectionViewCell")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       // profile_collection_view.register(UINib(nibName: "ProfileCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProfileCollectionViewCell")
        
       
//        let button = UIButton(type: .custom)
//        button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
//        button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
//        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
//        button.clipsToBounds = true
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.rightBarButtonItem = barButton
        
        user_data = (NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "user_data") as! Data) as! UserdataClass)
        if user_data.User_Type != 1 {
            
            add_new_profile_button.isHidden = true
        }
        
        add_new_profile_button.layer.masksToBounds = true
        add_new_profile_button.layer.cornerRadius = 20
        add_new_profile_button.addTarget(self, action: #selector(add_new_profile_action(_:)), for: .touchUpInside)
    
         profile_API()
        
    }
    @objc func add_new_profile_action(_ sender : UIButton) {
        
        let add_New_Profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewProfileViewController") as! AddNewProfileViewController
        
        self.navigationController?.pushViewController(add_New_Profile_VC, animated: true)
    }
    
    @objc
    
    func rightButtonAction(sender: Any) {
        
        displayAlert(msg: "Choose")
        
    }
    
    func displayAlert(msg:String) {
        
        let alert = UIAlertController()
        //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
            let modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ModeOfContactViewController") as! ModeOfContactViewController
            
            self.navigationController?.pushViewController(modeOfContact_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func profile_API() {
        
       let profile_API = APINAME.init().PROFILE_API
        
        print(profile_API)
        WebService.requestGetUrlWithoutParameters(strURL: profile_API, is_loader_required: true, success: { (response) in
            
            if response["status_code"] as! NSNumber == 1 {
            print(response)
            
                self.profile_Array = response["data"] as! [NSDictionary]
                
                print(self.profile_Array)
            }
            
            DispatchQueue.main.async {
                
                self.profile_collection_view.reloadData()
            }
            
        }) { (error) in
            
            print(error)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(profile_Array)
        return profile_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         let width = (self.view.frame.size.width - 60)/numberOfTimeCell
     //   let width =  (self.view.frame.size.width - 30)/numberOfTimeCell
        
      
        if user_data.User_Type != 1 {
            
            return CGSize(width: width, height: 220)
            
        }
        
        return CGSize(width: width, height: 250)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = profile_collection_view.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
        
        cell.main_view.layer.borderWidth = 1
        cell.main_view.clipsToBounds = true
        cell.main_view.layer.masksToBounds = true
        
        cell.user_image.layer.masksToBounds = true
        cell.user_image.clipsToBounds = true
        cell.user_image.contentMode = .scaleToFill
        cell.user_image.layer.cornerRadius = 38
      //  cell.user_image.layer.borderWidth = 1
        cell.user_name.text = ((profile_Array[indexPath.row] )["name"] as! String)
        cell.user_email.text = ((profile_Array[indexPath.row] )["email"] as! String)
       
 
            user_type =  ((profile_Array[indexPath.row] )["user_type"] as! NSNumber)
            
            if user_type == 1 {
                
                cell.user_type.text = "Admin"
                
            }
            if user_type == 2 {
                
                cell.user_type.text = "HR Manager"
                
            }
            if user_type == 3 {
                
                cell.user_type.text = "HR Recruiter"
                
            }
      
        if ((profile_Array[indexPath.row] )["status"] as! NSNumber) == 1{
            
            cell.user_status.text = "Enabled"
            cell.user_status.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            cell.user_status.layer.masksToBounds = true
            cell.user_status.layer.cornerRadius = 3
            
        }
        if ((profile_Array[indexPath.row] )["status"] as! NSNumber) == 0{
            
            cell.user_status.text = "Disabled"
            cell.user_status.textColor = #colorLiteral(red: 1, green: 0.09019607843, blue: 0.09803921569, alpha: 1)
            cell.user_status.layer.masksToBounds = true
            cell.user_status.layer.cornerRadius = 3
            
        }
        
        if user_data.User_Type != 1 {
            
            cell.edit_button.isHidden = true
            cell.delete_button.isHidden = true
            
        }
        
        cell.edit_button.addTarget(self, action: #selector(edit_button_action(_:)), for: .touchUpInside)
        cell.delete_button.addTarget(self, action: #selector(displayWarningAlert(_:)), for: .touchUpInside)
        
        if (profile_Array[indexPath.row].object(forKey: "profile_pic") as! String).isEmpty == true {
            
            //   cell.userImageView.setImage(string: user_data.user_first_name! + " " + user_data.user_last_name!)
            print("Image hai ni")
            
        }
        else
        {
            if imagePath.isEmpty
            {
                imagePath = (profile_Array[indexPath.row].object(forKey: "profile_pic") as! String)
            }
            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
            print(image_url as Any)
            
            cell.user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
            
            imagePath = ""
        }
        
        
        
        return cell
    }
    
    func delete_profile_API(index_path : Int) {
        
       profile_ID = (profile_Array[index_path].object(forKey: "id") as! NSNumber)
        
        let delete_API = ("\(APINAME.init().DELETE_PROFILE_API)/\(String(describing: profile_ID!))")
        
        print(delete_API)
        WebService.requestDelUrl(strURL: delete_API, is_loader_required: true, params: [:], success: { (response) in
            
            print(response)
            if response["status_code"] as! NSNumber == 1 {
                
                DispatchQueue.main.async {
                    
                    
                    self.profile_Array.remove(at: index_path)
                    self.profile_collection_view.reloadData()
                    self.displayDeleteAlert(msg: response["message"] as! String)
                    
                }
                
            }
            
            else {
                
                self.profile_collection_view.reloadData()
                self.displayDeleteAlert(msg: response["message"] as! String)
                
            }
            
        }) { (error) in
            
            print(error)
        }
        
        
    }
    
    func displayDeleteAlert(msg:String) {
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func displayWarningAlert(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.profile_collection_view)
        let indexPath = self.profile_collection_view.indexPathForItem(at: point)
        let cell = self.profile_collection_view.cellForItem(at: indexPath!) as! ProfileCollectionViewCell
        
        
        // let alert = UIAlertController()
        let alert = UIAlertController(title: "", message: "Are you sure?" , preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "delete", style: .default, handler: { (action) in
            
            self.delete_profile_API(index_path: (indexPath?.row)!)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action) in
            
            return
        }))
        
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
  
    @objc func edit_button_action(_ sender: UIButton) {
        
        let center = sender.center
        let point = sender.superview!.convert(center, to:self.profile_collection_view)
        let indexPath = self.profile_collection_view.indexPathForItem(at: point)
        //   let cell = self.home_collection_view.cellForItem(at: indexPath!) as! HomeCollectionViewCell
        
        
        let updateProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfileViewController") as! UpdateProfileViewController
        
        profile_ID = (profile_Array[(indexPath?.row)!].object(forKey: "id") as! NSNumber)
        updateProfileVC.profile_Array = profile_Array
        updateProfileVC.profile_ID = profile_ID
        updateProfileVC.profile_dict = (profile_Array[(indexPath?.row)!])
        self.navigationController?.pushViewController(updateProfileVC, animated: true)
        
    }
    
   

}
