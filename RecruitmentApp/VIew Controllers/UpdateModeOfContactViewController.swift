//
//  UpdateModeOfContactViewController.swift
//  Recruitment
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class UpdateModeOfContactViewController: UIViewController {

    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var update_button: UIButton!
    
    @IBOutlet weak var enter_title_text_field: UITextField!
    
    var modeOfContact_ID : NSNumber!
    var modeOfContact_Array = [NSDictionary] ()
    var modeOfContact_dict = NSDictionary()
    var modeOfContact_title = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        update_button.layer.masksToBounds = true
        update_button.layer.cornerRadius = 3
        enter_title_text_field.setBottomBorder(withColor: UIColor.lightGray)
        
        main_view.layer.borderWidth = 1
        
        
        //  add_button.addTarget(self, action: #selector(addNewDesignation_API(_:)), for: .touchUpInside)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Update Mode Of Contact"
        
        if modeOfContact_title == "" {
            
            modeOfContact_title = modeOfContact_dict.object(forKey: "title") as!String
        }
        
        enter_title_text_field.text = modeOfContact_title
        enter_title_text_field.delegate = self
        
        
        update_button.addTarget(self, action: #selector(updateModeOfContact_API(_:)), for: .touchUpInside)
        
        
        //   addNewDesignation_API()
    }
    
    @objc
    func updateModeOfContact_API(_ sender : UIButton) {
        
        if enter_title_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Title")
        }
        else {
            
            print(enter_title_text_field.text as Any)
            
            let update_modeOfContact_API = ("\(APINAME.init().UPDATE_MODE_OF_CONTACT_API)/\(String(describing: modeOfContact_dict.object(forKey: "id")!))")
            let params = ["title" : enter_title_text_field.text!]
            
            
            
            WebService.requestPUTUrlWithJSONDictionaryParameters(strURL: update_modeOfContact_API, is_loader_required: true, params: params, success: { (response) in
                
                
                print(response)
                
                if response["status_code"] as! NSNumber == 1 {
                    self.view.makeToast((response["message"] as! String), duration: 3.0, position: .top)
               self.navigationController?.popViewController(animated: true)
                    
                }
                else {
                    
                    self.displayAlert(msg: response["message"] as! String)
                    
                }
                
            }) { (error) in
                
                print(error)
                
            }
            
            
        }
        
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
//    func displayAlertSuccess(msg:String) {
//        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
//
//            self.navigationController?.popViewController(animated: true)
//
//        }))
//        let popOverController = alert.popoverPresentationController
//        popOverController?.sourceView = self.view
//        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
//        popOverController?.permittedArrowDirections = []
//        self.present(alert, animated: true, completion: nil)
//    }

}

extension UpdateModeOfContactViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        textField.returnKeyType = .done
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
        
    }
    
    
}
