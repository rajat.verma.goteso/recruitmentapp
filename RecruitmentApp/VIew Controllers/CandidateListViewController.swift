//
//  CandidateListViewController.swift
//  Recruitment
//
//  Created by Apple on 12/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class CandidateListViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout  {

    var numberOfTimeCell : CGFloat { if UIDevice.current.userInterfaceIdiom == .pad
    {   return 2    }
    else{        return 1 }
    }
    
    
    @IBOutlet weak var oops_image: UIImageView!
    
    var designation_dict = NSDictionary()
    var designation_Id : NSNumber!
    var candidate_Array = [NSDictionary] ()
    var imagePath = ""
    var window : UIWindow?
    var designation_title = ""
    
    @IBOutlet weak var add_button: UIButton!
    @IBOutlet weak var candidate_collection_view: UICollectionView!
    override func viewDidLoad() {
        
        super.viewDidLoad()

        candidate_collection_view.register(UINib(nibName: "CandidateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CandidateCollectionViewCell")
       
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        designation_title = designation_dict["name"] as! String
        
        self.candidate_collection_view.reloadData()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = designation_title
        
        
        addBackButton()
        
        add_button.layer.masksToBounds = true
        add_button.layer.cornerRadius = 20
        add_button.addTarget(self, action: #selector(add_button_action(_:)), for: .touchUpInside)
//        let button = UIButton(type: .custom)
//        button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
//        button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
//        button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
//        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
//        button.clipsToBounds = true
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.rightBarButtonItem = barButton
        
        
         call_candidate_API()
        
        
      
        
        
    }
    
    @objc func linkedin_button_action(_ sender : UIButton) {
        
         let index  = sender.tag
        
        print(self.candidate_Array[index])
        
        if ((self.candidate_Array[index] )["linkedin"] as! String) == "" {
    
            displayAlert(msg: "Linkdin is empty")
            
        }
        
        else{
            
          if let linkedinUrl = URL(string: ((self.candidate_Array[index] )["linkedin"] as! String)),
          UIApplication.shared.canOpenURL(linkedinUrl) {
          UIApplication.shared.open(linkedinUrl, options: [:], completionHandler: nil)
            
             }
           }
        
         }
    
    @objc func facebook_button_action(_ sender : UIButton) {
        
        let index  = sender.tag
        
        print(self.candidate_Array[index])
        
        if ((self.candidate_Array[index] )["facebook"] as! String) == "" {
            
            displayAlert(msg: "Facebook is empty")
            
        }
        
        else {
            
            let facebookURL = NSURL(string: ((self.candidate_Array[index] )["facebook"] as! String))
            if UIApplication.shared.canOpenURL(facebookURL! as URL) {
                UIApplication.shared.openURL(facebookURL! as URL)
            } else {
            UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/PageName")! as URL)
            }
      
        }
        
    }
    
    @objc func twitter_button_action(_ sender : UIButton) {
    
        
        
        let index  = sender.tag
        
        print(self.candidate_Array[index])
        
        if ((self.candidate_Array[index] )["twitter"] as! String) == "" {
            
            displayAlert(msg: "Twitter is empty")
            
        }
        else {
        
        let twitterURL = NSURL(string: ((self.candidate_Array[index] )["twitter"] as! String))!
    if UIApplication.shared.canOpenURL(twitterURL as URL) {
        UIApplication.shared.openURL(twitterURL as URL)
        }
    else {
        UIApplication.shared.openURL(NSURL(string: "https://twitter.com/USERNAME")! as URL)
        }
    
        }
    }
    
    @objc
    func whatsapp_button_action(_ sender : UIButton) {
        
        let index  = sender.tag
        
        print(self.candidate_Array[index])
        
        if ((self.candidate_Array[index] )["whatsapp"] as! String) == "" {
            
            displayAlert(msg: "WhatsApp is empty")
            
        }
        
        else {
        
        let phoneNumber = ((self.candidate_Array[index] )["whatsapp"] as! String) // you need to change this number
        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
        if UIApplication.shared.canOpenURL(appURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
        } else {
            // WhatsApp is not installed
            displayAlert(msg: "WhatsApp is not installed")
            
        }
            
        }
        
    }
    
    @objc func add_button_action(_ sender : UIButton) {
        
        
        
        let add_candidate_VC = self.storyboard?.instantiateViewController(withIdentifier: "AddCandidateViewController") as! AddCandidateViewController
        
        add_candidate_VC.designation = ("\(designation_dict["id"] as! NSNumber)")
        
        self.navigationController?.pushViewController(add_candidate_VC, animated: true)
    }
    
    func call_candidate_API() {
        
        let candidate_Api = ("\(APINAME.init().CANDIDATE_API)?designation_id=\(String(describing: designation_dict["id"] as! NSNumber))")
        
        print(candidate_Api)
        WebService.requestGetUrlWithoutParameters(strURL: candidate_Api, is_loader_required: true, success: { (response) in
            
            print(response)
            
            if response["status_code"] as! NSNumber == 1 {
             
            
          self.candidate_Array = response["data"] as! [NSDictionary]
            
            print(self.candidate_Array)
                
                if  self.candidate_Array.count == 0 {
                    
                    self.oops_image.isHidden = false
                    
                }
                else {
                    
                    self.oops_image.isHidden = true
                }
                
            
            DispatchQueue.main.async {
                
                self.candidate_collection_view.reloadData()
            }
                
            }
            
            else {
                
                self.displayAlert(msg: response["message"] as! String)
            }
            
        }) { (error) in
            
            print(error)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
     
        
        candidate_collection_view.register(UINib(nibName: "CandidateCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "CandidateCollectionReusableView")
        
        
        let headerView = candidate_collection_view.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "CandidateCollectionReusableView", for: indexPath) as! CandidateCollectionReusableView
        
     
//
//        if let layout = candidate_collection_view.collectionViewLayout as? UICollectionViewFlowLayout {
//            layout.scrollDirection = .horizontal
//        }
        
        
    
        return headerView
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
       
            return CGSize(width: self.view.frame.size.width, height: 80)
      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return candidate_Array.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //  let width = (self.view.frame.size.width - 60)/numberOfTimeCell
        let width =  (self.view.frame.size.width - 60)/numberOfTimeCell
        
        return CGSize(width: width, height: 450)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = candidate_collection_view.dequeueReusableCell(withReuseIdentifier: "CandidateCollectionViewCell", for: indexPath) as! CandidateCollectionViewCell
        
        cell.linkedin_button.tag = indexPath.row
        cell.linkedin_button.addTarget(self, action: #selector(linkedin_button_action(_:)), for: .touchUpInside)
        
        cell.whatsapp_button.tag = indexPath.row
        cell.whatsapp_button.addTarget(self, action: #selector(whatsapp_button_action(_:)), for: .touchUpInside)
        
        cell.twitter_button.tag = indexPath.row
        cell.twitter_button.addTarget(self, action: #selector(twitter_button_action(_:)), for: .touchUpInside)
        
        cell.facebook_button.tag = indexPath.row
        cell.facebook_button.addTarget(self, action: #selector(facebook_button_action(_:)), for: .touchUpInside)
        
        
        if let user_name = (candidate_Array[indexPath.row] )["name"] as? String{
            
              cell.user_name.text = user_name
        }
        
        else {
            
             cell.user_name.text = ""
            
        }
        
        
        if let user_gender = (candidate_Array[indexPath.row] )["gender"] as? String  {
            
            cell.user_gender.text = user_gender
        }
        else {
            
            cell.user_gender.text = ""
        }
        
        if let user_current_location = (candidate_Array[indexPath.row] )["current_address"] as? String {
            
            cell.user_current_location.text = user_current_location
        }
        else {
            
             cell.user_current_location.text = ""
            
        }
        if let user_email = (candidate_Array[indexPath.row] )["email"] as? String {
            
            cell.user_email.text = user_email
        }
        
        else {
            
             cell.user_email.text = ""
        }
        
        if let user_phone = (candidate_Array[indexPath.row] )["mobile"] as? String{
            
            cell.user_phone_label.text = ("\(user_phone)")
          //  cell.user_phone.setTitle(("\(user_phone)"), for: .normal)
        }
        else {
            
            cell.user_phone_label.text = ""
        }
            
        if let user_whatsapp = (candidate_Array[indexPath.row] )["whatsapp"] as? String  {
            //cell.whatsapp_button.addTarget(self, action: #selector(whatsapp_button_action(_:)), for: .touchUpInside)
            cell.user_whatsapp_label.text = ("\(user_whatsapp)")
        //   cell.user_whatsapp.setTitle(("\(user_whatsapp)"), for: .normal)
        }
        else {
            
            cell.user_whatsapp_label.text = ""
        }
        
    
       
        cell.main_view.layer.masksToBounds = true
        
        cell.user_name.numberOfLines = 0
     
        cell.main_view.layer.borderWidth = 1
        cell.main_view.clipsToBounds = true
        cell.user_image.layer.masksToBounds = true
        cell.user_image.layer.cornerRadius = 30
       
        cell.user_image.contentMode = .scaleToFill
        cell.user_image.layer.isOpaque = false
        cell.user_image.clipsToBounds = true
        
        

       
          cell.view_more_button.layer.masksToBounds = true
        cell.view_more_button.layer.cornerRadius = 3
        
        cell.view_more_button.addTarget(self, action: #selector(view_more_action(_sender:)), for: .touchUpInside)
        
       
        if (candidate_Array[indexPath.row].object(forKey: "profile_pic") as! String).isEmpty == true {
            
            print("Image hai ni")
            
        }
        else
        {
            if imagePath.isEmpty
            {
                imagePath = (candidate_Array[indexPath.row].object(forKey: "profile_pic") as! String)
            }
            let image_url = URL(string: BASE_IMAGE_URL + imagePath)
            print(image_url as Any)
            
            cell.user_image.sd_setImage(with: image_url, placeholderImage:UIImage(named: "placeHolderImage"))
            
            imagePath = ""
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let candidate_all_detail_VC = self.storyboard?.instantiateViewController(withIdentifier: "CandidateAllDetailViewController") as! CandidateAllDetailViewController
        candidate_all_detail_VC.id = ((candidate_Array[indexPath.row] )["id"] as! NSNumber)
        candidate_all_detail_VC.designation = (designation_dict["id"] as! NSNumber)
        
        self.navigationController?.pushViewController(candidate_all_detail_VC, animated: true)
    }
    
 
    
    @objc func view_more_action(_sender: UIButton) {
        
        let center = _sender.center
        let point = _sender.superview!.convert(center, to:self.candidate_collection_view)
        let indexPath = self.candidate_collection_view.indexPathForItem(at: point)
       // let cell = self.candidate_collection_view.cellForItem(at: indexPath!) as! CandidateCollectionViewCell
        
        let candidate_all_detail_VC = self.storyboard?.instantiateViewController(withIdentifier: "CandidateAllDetailViewController") as! CandidateAllDetailViewController
        candidate_all_detail_VC.id = ((candidate_Array[indexPath!.row] )["id"] as! NSNumber)
        candidate_all_detail_VC.designation = (designation_dict["id"] as! NSNumber)
        
        self.navigationController?.pushViewController(candidate_all_detail_VC, animated: true)
        
    }
    
    @objc
    func phone_action(_ sender: UIButton) {
//
//        let phoneNumber =  "+989160000000" // you need to change this number
//        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
//        if UIApplication.shared.canOpenURL(appURL) {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
//            }
//            else {
//                UIApplication.shared.openURL(appURL)
//            }
//        } else {
//            // WhatsApp is not installed
//            displayAlert(msg: "WhatsApp is not installed")
//
//        }
        
        displayAlertSelect(msg: "Select")
        
    }
    
    func sendMessageToNumber(number:String,message:String){
        
        let sms = "sms:\(number)&body=\(message)"
        let url = URL(string:sms)!
        let shared = UIApplication.shared
        
        if(shared.canOpenURL(url)){
           UIApplication.shared.open(URL.init(string: sms.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!, options: [:], completionHandler: nil)
        }
        else{
            
           displayAlert(msg: "unable to send message")
            print("unable to send message")
        }
    }
    
    func displayAlertSelect(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Message", style: .default, handler: { (action) in
            
            self.sendMessageToNumber(number: "9041924249", message: "hello frnds chai pee lo")
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Phone", style: .default, handler: { (action) in
            
            
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc
    func rightButtonAction(sender: Any) {
        
        displayAlertSettings(msg: "Choose")
        
    }
    
    func displayAlertSettings(msg:String) {
        
        let alert = UIAlertController()
        //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
            
            let designation_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
            
            self.navigationController?.pushViewController(designation_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
            let modeOfContact_VC = self.storyboard?.instantiateViewController(withIdentifier: "ModeOfContactViewController") as! ModeOfContactViewController
            
            self.navigationController?.pushViewController(modeOfContact_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action) in
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let LoginViewController = storyboard.instantiateViewController(withIdentifier: "initController") as! UINavigationController
            
            self.window?.rootViewController = LoginViewController
            self.window?.makeKeyAndVisible()
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
}
