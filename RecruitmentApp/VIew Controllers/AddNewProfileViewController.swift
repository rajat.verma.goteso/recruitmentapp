//
//  AddNewProfileViewController.swift
//  Recruitment
//
//  Created by Apple on 11/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class AddNewProfileViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate {

    
    @IBOutlet weak var user_type_button: UIButton!
    @IBOutlet weak var user_image: UIImageView!
    
    @IBOutlet weak var edit_user_image: UIButton!
    
    
    @IBOutlet weak var user_email_text_field: UITextField!
    @IBOutlet weak var user_name_text_field: UITextField!
    
    @IBOutlet weak var update_button: UIButton!
    @IBOutlet weak var user_confirm_password_text_field: UITextField!
    @IBOutlet weak var user_password_text_field: UITextField!
    @IBOutlet weak var user_mobile_text_field: UITextField!
    @IBOutlet weak var user_type_text_field: UITextField!
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    let button = UIButton(type: .custom)
    var name = ""
    var image_url = ""
    var imagePath = ""
    var userImage:UIImage?
    var image_data:AnyObject!
     var components = ["HR Manager","Recruiter"]
    
    var user_type = NSNumber()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        addBackButton()
        self.navigationItem.title = "Add Profile"
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        button.addTarget(self, action: #selector(rightButtonAction(sender:)), for: UIControl.Event.touchUpInside)
        button.setBackgroundImage(UIImage(named: "settings"), for: .normal)
        //  button.setImage(UIImage(named: "plusBtnImage"), for: UIControl.State.normal)
        button.clipsToBounds = true
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
        
        
        
        if userImage != nil
        {
            user_image.image = userImage
            user_image.contentMode = .scaleToFill
        }
        
        user_type_text_field.rightView = self.btnDropDown
        user_type_text_field.rightViewMode = .always
        user_type_text_field.isUserInteractionEnabled = false
        user_type_text_field.rightView?.isUserInteractionEnabled = false
        user_type_button.isHidden = false
        user_type_button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        
        user_name_text_field.delegate = self
        user_email_text_field.delegate = self
        user_mobile_text_field.delegate = self
        user_type_text_field.delegate = self
        user_password_text_field.delegate = self
        user_confirm_password_text_field.delegate = self
        
        edit_user_image.addTarget(self, action: #selector(edit_image_button_action(_:)), for: .touchUpInside)
        update_button.addTarget(self, action: #selector(addNewProfile_API(_:)), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        
        user_type_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_name_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_email_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_mobile_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
        user_confirm_password_text_field.setBottomBorder(withColor: UIColor.lightGray)
        
    }
    
    internal var btnDropDown: UIButton {
        let size: CGFloat = 20.0
        
        button.setImage(UIImage(named: "drop_down"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -(size/2.0), bottom: 0, right: 0)
        button.frame = CGRect(x: view.frame.size.width - size, y: 0.0, width: 10, height: size)
        button.addTarget(self, action: #selector(YOUR_BUTTON__TAP_ACTION(_:)), for: .touchUpInside)
        return button
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return components.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return components[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        user_type_text_field.text = components[row]
        
        self.view.endEditing(true)
    }
    
    @objc func YOUR_BUTTON__TAP_ACTION(_ sender: UIButton) {
        picker = UIPickerView.init()
        picker.delegate = self
        //   picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.setValue(UIColor.white, forKey: "backgroundColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.tintColor = UIColor.black
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
    }
    
    @objc func onDoneButtonTapped() {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
    }
    
    @objc
    
    func rightButtonAction(sender: Any) {
        
        displayAlert_2(msg: "Choose")
        
    }
    
    @objc
    func addNewProfile_API(_ sender : UIButton) {
        
        if user_name_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Name")
        }
        if user_email_text_field.text == "" {
            
            self.displayAlert(msg: "Enter Email")
            
        }
        if user_type_text_field.text == "" {
            
            self.displayAlert(msg: "Enter user_type")
        }
        
        if user_mobile_text_field.text == "" {
            
            self.displayAlert(msg: "Enter mobile")
        }
         
        if user_password_text_field.text == "" {
            
            self.displayAlert(msg: "Enter password")
        }
            
        if user_confirm_password_text_field.text == "" {
           
            self.displayAlert(msg: "confirm password")
        }
            
        if user_confirm_password_text_field.text != user_password_text_field.text {
            
            self.displayAlert(msg: "Your password and confirmation password do not match.")
            
        }
            
        else {
            
      //      print(enter_designation_text_field.text as Any)
            if imagePath != "" {
                
                if user_type_text_field.text == "Admin" {
                    
                    user_type = 1
                }
                if user_type_text_field.text == "HR Manager" {
                    
                    user_type = 2
                }
                if user_type_text_field.text == "HR Recruiter" {
                    
                    user_type = 3
                }
                
                let new_Profile_API = APINAME.init().ADD_NEW_PROFILE_API
                let params = ["name" : user_name_text_field.text! ,"email" : user_email_text_field.text! , "mobile" : user_mobile_text_field.text! , "user_type" : user_type , "password" : user_password_text_field.text! , "password_confirmation" : user_confirm_password_text_field.text! ,"profile_pic" : imagePath] as [String : Any]
                
                print(imagePath)
                
                WebService.requestPostUrl(strURL: new_Profile_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
                    
                    print(response)
                    
                    if response["status_code"] as! NSNumber == 1 {
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    else {
                        
                        self.displayAlert(msg: response["message"] as! String)
                        
                    }
                    
                }) { (error) in
                    
                    
                    print(error)
                }
                
            }
                
            else {
                
                displayAlert(msg: "Please select image")
                
            }
            
        }
        
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlert_2(msg:String) {
        
        let alert = UIAlertController()
        //     let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Designation", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "DesignationViewController") as! DesignationViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Mode of Contact", style: .default, handler: { (action) in
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            
            let profile_VC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            self.navigationController?.pushViewController(profile_VC, animated: true)
            return
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            return
        }))
        
        
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }

}

extension AddNewProfileViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        textField.returnKeyType = .done
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print(newString)
      
        if textField.placeholder == "Name" {
            
            let userInput = newString
            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
            if userInput.rangeOfCharacter(from: set.inverted) != nil {
                print("ERROR: There are numbers included!")
                displayAlert(msg: "Name requires alphabets only.")
                
            }
            else {
                
             print(newString)
            }
        }
        
        return true
       
    }
   
}

extension AddNewProfileViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBAction func edit_image_button_action(_ sender: UIButton) {
        
        let imageController = UIImagePickerController()
        imageController.delegate = self
        
        imageController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(imageController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //  let image : UIImage = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage = pickedImage
            
        }
        self.dismiss(animated: true, completion: nil)
        
        image_data = userImage?.jpegData(compressionQuality: 0.5) as AnyObject
        apiMultipart(image_data: image_data)
        // apiMultipart(image_data: user_image)
        
    }
    
    func apiMultipart(image_data : AnyObject?) {
        
        let image_upload_API = APINAME.init().UPLOAD_IMAGE_API
        
        let serviceName = BASE_URL + image_upload_API
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = image_data {
                
                multipartFormData.append(data as! Data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
                
            }
        }, usingThreshold: UInt64.init(), to: serviceName, method: .post, headers: [:]) { (result) in
            
            switch result{
                
            case .success(let upload, _, _) :
                upload.responseJSON { response in
                    print("Succesfully uploaded  = \(response)")
                    
                    if let err = response.error{
                        SVProgressHUD.dismiss()
                        print(err)
                        return
                    }
                    else
                    {
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            if JSON["status_code"] as! NSNumber == 0 {
                                
                                self.displayAlert(msg: JSON["message"] as! String)
                                
                            }
                            
                            else {
                                
                                self.imagePath = (JSON["data"] as! NSDictionary).object(forKey: "image_url") as! String
                                print(self.imagePath)
                                DispatchQueue.main.async {
                                    let image_url = URL(string: BASE_IMAGE_URL +  self.imagePath )
                                    if image_url != nil
                                    {
                                        if let data1 = NSData(contentsOfFile: BASE_IMAGE_URL + self.imagePath )
                                        {
                                            self.userImage = UIImage(data: data1 as Data)
                                        }
                                        
                                    }
                                    
                                    SVProgressHUD.dismiss()
                                    self.reloadInputViews()
                                    //     self.tableView.reloadData()
                                }
                                
                                print(JSON)
                            }
                        }
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)              }
            
        }
        
    }
    
    
}
