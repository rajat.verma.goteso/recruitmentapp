//
//  ResetPasswordViewController.swift
//  Recruitment
//
//  Created by Apple on 04/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var Forgot_password_btn: UIButton!
    
    
    @IBOutlet weak var email_text_field: UITextField!
    
    @IBAction func forgot_password_action(_ sender: UIButton) {
        
        if email_text_field.text?.isEmpty == true {
            
            displayAlert(msg: "Enter Email")
            
        }
        
        else{
        
        reset_password_API()
        
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title =  "Reset Password"
        
        email_text_field.setBottomBorder(withColor: UIColor.lightGray)
        addBackButton()
        
        self.Forgot_password_btn.layer.masksToBounds = true
        self.Forgot_password_btn.layer.cornerRadius = 3
        
      
        
    }
    
    override func viewDidLayoutSubviews() {
        
        email_text_field.setBottomBorder(withColor: UIColor.lightGray)
        email_text_field.delegate = self
    }
    
    func reset_password_API() {
        
        let resetPassword_API = APINAME.init().FORGOT_PASSWORD_API
        
        let params = ["email" : email_text_field.text!] as [String : Any]
        
        WebService.requestPostUrl(strURL: resetPassword_API, params: params as NSDictionary, is_loader_required: true, success: { (response) in
            
            if response["status_code"] as! NSNumber == 1 {
                
                self.displayAlertAction(msg: response["message"] as! String)
                
                
            }
            
            else {
                
                self.displayAlert(msg: response["message"] as! String)
                
            }
            
        }) { (error) in
            
            
            print(error)
        }
        
    }
    
    func displayAlert(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlertAction(msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
       
            let verifyOtp_VC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOtpViewController") as! VerifyOtpViewController
            
            verifyOtp_VC.email = self.email_text_field.text!
            self.navigationController?.pushViewController(verifyOtp_VC, animated: true)
            
            
            return
        }))
        let popOverController = alert.popoverPresentationController
        popOverController?.sourceView = self.view
        popOverController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popOverController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: nil)
    }
    
    
    

}

extension ResetPasswordViewController :  UITextFieldDelegate {
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func isTextFieldValid(string: String, textField: UITextField)   {
        
        let alertController = UIAlertController(title: "", message: "Enter " + string, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            // textField.becomeFirstResponder()
            return
        }))
        let popPresenter = alertController.popoverPresentationController
        popPresenter?.sourceView = self.view
        popPresenter?.sourceRect = self.view.bounds
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        textField.textColor = #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1)
        textField.setBottomBorder(withColor: #colorLiteral(red: 0.2196078431, green: 0.5294117647, blue: 0.7725490196, alpha: 1))
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor.black
        textField.setBottomBorder(withColor: UIColor.lightGray)
        return
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        return true
        
        
    }
    
    
    
}
